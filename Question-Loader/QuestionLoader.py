import sqlite3
import requests
from html import unescape

URL = "https://opentdb.com/api.php?type=multiple&"
PAR_AMOUNT = "amount="
PAR_DIFFICULTY = "difficulty="

HTTP_SUCCESS = 200

DATABASE_PATH = "..\\sqlite3\\Trivia_DB.sqlite"


def load_questions_to_database(question_list):
    db = sqlite3.connect(DATABASE_PATH)

    for question in question_list:
        column_question = unescape(question.get("question")).replace('"', "'")
        column_correct_answer = unescape(question.get("correct_answer")).replace('"', "'")
        column_wrong_answers_json = unescape(str(question.get("incorrect_answers"))).replace('"', "'")

        sql = "INSERT INTO Questions (question, correct_answer, wrong_answers_json) VALUES (\"{0}\", \"{1}\", \"{2}\");".format(column_question, column_correct_answer, column_wrong_answers_json)

        db.execute(sql)

    db.commit()
    db.close()


def get_questions(params):
    res = requests.get(URL + params)

    if res.status_code != HTTP_SUCCESS:
        raise Exception("Error: could not get questions")

    return dict(res.json())


def main():
    params = PAR_AMOUNT
    params = params + input("Enter the number of questions to load (max value is 50): ")
    params = params + "&"

    choose = input("""Select the difficulty of the questions:
1. Random
2. Easy
3. Medium
4. Hard
""")

    if choose == '2':
        params = params + PAR_DIFFICULTY + "easy"
    elif choose == '3':
        params = params + PAR_DIFFICULTY + "medium"
    elif choose == '4':
        params = params + PAR_DIFFICULTY + "hard"

    try:
        questions_dict = get_questions(params)
        load_questions_to_database(questions_dict.get("results"))

        input("\nLoad complete!")

    except Exception as ex:
        print(ex)
        input()


if __name__ == '__main__':
    main()
