if __name__ == "__main__":
    from gevent import monkey,socket
    monkey.patch_all()
    import bson
    bson.patch_socket()
    from CommunicationTester import main
    main()