import socket
import bson

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65432        # The port used by the server

NUM_OF_BYTES_IN_LEN = 4
NUM_OF_BITS_IN_BYTE = 8

SIGN_UP_CODE = 4
LOGIN_CODE = 3

LOGIN_MSG = {"password":"1234","username":"user1"}
SIGNUP_MSG = {"username": "user1", "password": "1234", "email": "user1@gmail.com"}

def dec_to_bin(dec_num):
    """
        function convert decimal number from base 10 to binary base 2
    """
    return str(bin(int(dec_num))[2:]).zfill(8)

def string_to_binary(string):
    """
        function convert text to binary base 2
    """
    return ''.join(format(ord(x), 'b').zfill(8) for x in string)

def create_client_msg(client_msg, code): 
    """
        function encode the json to binary and add the len and the code
    """
    b_data = bson.dumps(client_msg).decode()

    return chr(code) + b_data


def menu():
    choice = input(""" 
    what msg would you like to send
    press 1 for login
    press anything else for sign up
    """)
    return choice

def bin_to_dec(binary):
    return int(binary, 2)

def bin_to_text(binary):
    """
        function split the string to 8 each and convert it
    """
    i = 0
    data = ""
    while i < len(binary):
        data += chr(bin_to_dec(binary[i:i+NUM_OF_BITS_IN_BYTE]))
        i += NUM_OF_BITS_IN_BYTE
    return data

def divide_list(start_list, n): 
    divided = []
    for i in range(0, len(start_list), n):  
        divided.append(start_list[i:i+n])
    return divided

def decode_server_data(server_data):
    server_data = server_data.split(chr(0))
    server_data = [x for x in server_data if x] #  remove the empty strings
    server_data = server_data[1:] # remove the size of the json
    server_data = divide_list(server_data, 2)
    return server_data

def main():
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((HOST, PORT))
    except Exception:
        print("couldn't conenct to server")
    
    try:
        if menu() == "1":
            client_msg = create_client_msg(LOGIN_MSG, LOGIN_CODE)
        else:
            client_msg = create_client_msg(SIGNUP_MSG, SIGN_UP_CODE)
        client_socket.sendall(client_msg.encode())  
    except Exception:
        print("couldn't send the message")

    try:
        msg = client_socket.recv(1024).decode()

        # print each decoded data from the server msg
        msg = decode_server_data(msg)
        if len(msg) == 1: # status 1
            data = msg[0]
            print(data[0][1:], ": ", ord(data[1]))
        else: # status error
            print(msg[0][0][1:], ": ", msg[1][0])
            
            
    except Exception as e:
        print("couldn't recieved the message", e)