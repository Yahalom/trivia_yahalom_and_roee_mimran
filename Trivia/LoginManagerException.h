#pragma once
#include "TriviaException.h"

class LoginManagerException : public TriviaException
{
public:
	LoginManagerException(const std::string& data) : TriviaException(data) {}
};