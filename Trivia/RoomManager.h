#pragma once
#include "Constants.h"
#include "Room.h"

typedef std::pair<unsigned int, Room> room_map_entry;
typedef std::unordered_map<unsigned int, Room> room_map;

class RoomManager
{
public:
	RoomManager();

	unsigned int createRoom(LoggedUser user, RoomData data);
	void deleteRoom(unsigned int roomId);
	bool isRoomActive(unsigned int roomId);
	std::vector<RoomData> getRooms();
	Room& getRoom(unsigned int roomId);
	void closeRoom(unsigned int roomId);

private:
	room_map _rooms;
	unsigned int _roomCounter;

	room_map::iterator findRoom(unsigned int roomId);
};