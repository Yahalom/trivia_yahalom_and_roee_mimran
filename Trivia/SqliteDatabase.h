#pragma once
#include "Constants.h"
#include "IDatabase.h"
#include "../sqlite3/sqlite3.h"
#include "Helper.h"
#include <mutex>

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	virtual bool doesUserExist(const std::string& username);
	virtual bool doesPasswordMatch(const std::string& username, const std::string& password);

	virtual bool addNewUser(const User& user);

	virtual userStatistics getUserStatistics(const std::string& username);
	virtual user_statistics_list getTopUsers(unsigned int numOfusers);

	virtual void updateUserStatistics(const inGameUserStatistics& gameStats);

	virtual questions getQuestions(unsigned int numOfQuestions);

private:
	sqlite3* _database;
	table _table;

	std::mutex _tableMutex;
	std::unique_lock<std::mutex> _tableULock;

	int getUserId(const std::string& username);

	void executeOutputQuery(const std::string& query, table& outputTable);
	void executeInputQuery(const std::string& query);

	void transaction();
	void rollback();
	void commit();

	void lockTable();
	void unlockTable();
};

