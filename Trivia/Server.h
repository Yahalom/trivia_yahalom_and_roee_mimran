#pragma once
#include "Communicator.h"

#define COMMAND_EXIT "EXIT"

class Server
{
public:
	Server();

	void run();

private:
	Communicator _communicator;
	RequestHandlerFactory _factory;
};

