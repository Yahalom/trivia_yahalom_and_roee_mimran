#pragma once
#include "TriviaException.h"

class GameException : public TriviaException
{
public:
	GameException(const std::string& data) : TriviaException(data) {}
};