#pragma once
#include "ThreadException.h"
#include "SocketException.h"
#include "SerializerException.h"
#include "DeserializerException.h"
#include "MessageException.h"
#include "DatabaseException.h"
#include "LoginManagerException.h"
#include "RoomException.h"
#include "GameException.h"

#include "Question.h"
#include "User.h"
#include "LoggedUser.h"
#include "../json-master/single_include/nlohmann/json.hpp"
#include <chrono>

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Communication:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

using json = nlohmann::json;

typedef std::vector<unsigned char> buffer;

#define SERVER_PORT 65432

#define MAX_DATA_SIZE 1000

#define BYTE_SIZE 8

#define DATA_LEN_BYTE_SIZE 4
#define CODE_BYTE_SIZE 1

#define RECV_END 205


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Database:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

typedef json row;
typedef std::vector<row> table;

#define SQLITE_DATABASE_PATH "../sqlite3/Trivia_DB.sqlite"

#define ERR_COLUMN_NOT_FOUND "-1"

// Tables:
#define TABLE_USERS "Users"
#define TABLE_STATISTICS "Statistics"
#define TABLE_QUESTIONS "Questions"

// Users columns:
#define COLUMN_USER_ID "user_id"
#define COLUMN_USERNAME "username"
#define COLUMN_PASSWORD "password"
#define COLUMN_EMAIL "email"

// Statistics columns:
#define COLUMN_TOTAL_ANSWER_TIME "total_answer_time"
#define COLUMN_HIGH_SCORE "high_score"
#define COLUMN_NUM_OF_ANSWERS "number_of_answers"
#define COLUMN_NUM_OF_CORRECT_ANSWERS "number_of_correct_answers"
#define COLUMN_NUM_OF_GAMES "number_of_games"
#define COLUMN_NUM_OF_GAMES_WON "number_of_games_won"

// Questions columns:
#define COLUMN_QUESTION_ID "question_id"
#define COLUMN_QUESTION "question"
#define COLUMN_CORRECT_ANSWER "correct_answer"
#define COLUMN_WRONG_ANSWERS_LIST "wrong_answers_json"

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Statistics:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

struct userStatistics
{
	std::string username;
	float totalAnswerTime;
	int highScore;
	int numOfAnswers;
	int numOfCorrectAnswers;
	int numOfGames;
	int numOfGamesWon;
};

struct inGameUserStatistics
{
	std::string username;
	float totalInGameAnswerTime;
	int inGameScore;
	int inGameCorrectAnswerCount;
	int inGameWrongAnswerCount;
	bool won;
};

typedef std::vector<userStatistics> user_statistics_list;

#define NUM_OF_USERS_IN_TOP_SCORE 3

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Game:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

struct GameData
{
	unsigned int currentQuestionIndex;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float totalAnswerTime;
	bool isInGame;
	std::chrono::steady_clock::time_point startTime; // An helper "time_point" to calculate the answer time.
};

#define MAX_QUESTIONS 5
#define TIME_OVER 5

typedef std::pair<LoggedUser, GameData> inGamePlayer;
typedef std::map<LoggedUser, GameData> inGamePlayers;


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
// unused code:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
// An hash function that lets "LoggedUser" to be used as an "unordered_map" key.
namespace std
{
	template <>
	struct hash<LoggedUser>
	{
		std::size_t operator()(const LoggedUser& user) const
		{
			return hash<string>()(user.getUsername()) ^ hash<SOCKET>()(user.getSocket());
		}
	};
}*/