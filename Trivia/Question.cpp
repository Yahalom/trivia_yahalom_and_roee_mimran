#include "Question.h"

/*
A constructor for class Question.
Input:
	question - A question.
	correctAnswer - The correct answer;
	allAnswers - All the answers (including the correct one).

Output:
	none.
*/
Question::Question(const std::string& question, const std::string& correctAnswer, const answers& allAnswers)
{
	this->_question = question;
	this->_correctAnswer = correctAnswer;
	this->_answers = allAnswers;
}

/*
A method returns the question.
Input:
	none.

Output:
	The question.
*/
const std::string Question::getQuestion() const
{
	return this->_question;
}

/*
A method returns all the answers.
Input:
	none.

Output:
	All the answers.
*/
const answers Question::getAnswers() const
{
	return this->_answers;
}

/*
A method returns the correct answer.
Input:
	none.

Output:
	The correct answer.
*/
const std::string Question::getCorrectAnswer()
{
	return this->_correctAnswer;
}