#pragma once

#include "StructRequests.h"
#include "WSAInitializer.h"

// prevent circular dependency from StructRequests.h
struct RequestResult;
struct RequestInfo;
struct LoginRequest;
struct SignupRequest;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo requestInfo, SOCKET clientSocket) = 0;
};

