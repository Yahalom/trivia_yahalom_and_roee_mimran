#include "RoomManager.h"

/*
A constructor for class RoomManager.
Input:
	none.

Output:
	none.
*/
RoomManager::RoomManager()
{
	this->_roomCounter = 0;
}

/*
A method that adds a new room to the room map.
Input:
	user - The user that opened the room.
	maxPlayers - The maximum number of users that can enter the room.
	maxPlayers - The maximum number of users that can enter the room.
	timePerQuestion - The time to answer evert question in the room. 

Output:
	room id
*/
unsigned int RoomManager::createRoom(LoggedUser user, RoomData data)
{
	for (auto it = this->_rooms.begin(); it != this->_rooms.end(); ++it)
	{
		if (it->second.getData().isActive && it->second.getData().name == data.name)
		{
			throw RoomException("Error: a room with the name '" + data.name + "' already exists");
		}
	}

	data.id = this->_roomCounter;
	

	Room room(data);
	room.addUser(user);

	this->_rooms.insert(room_map_entry(this->_roomCounter, room));

	this->_roomCounter++;
	return data.id;
}

/*
A method that deletes a room from the room map.
Input:
	roomId - The id of the room to delete.

Output:
	none.
*/
void RoomManager::deleteRoom(unsigned int roomId)
{
	this->_rooms.erase(this->findRoom(roomId));
}

/*
A method that checks if a room is active.
Input:
	roomId - The id of the room to be checked.

Output:
	none.
*/
bool RoomManager::isRoomActive(unsigned int roomId)
{
	return this->findRoom(roomId)->second.getData().isActive;
}

/*
A method that returns all the data of the rooms in the room map.
Input:
	none.

Output:
	none.
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> rooms;

	for (auto it = this->_rooms.begin(); it != this->_rooms.end(); ++it)
	{
		if (it->second.getData().isActive)
		{
			rooms.push_back(it->second.getData());
		}
	}

	return rooms;
}

/*
function get the room according to the room id
input:
	roomId the id of the room we want

output:
	the room we want to add the user
*/
Room& RoomManager::getRoom(unsigned int roomId)
{
	return this->findRoom(roomId)->second;
}

/*
A method that closes a room.
Input:
	none.

Output:
	none.
*/
void RoomManager::closeRoom(unsigned int roomId)
{
	this->findRoom(roomId)->second.close();
}

room_map::iterator RoomManager::findRoom(unsigned int roomId)
{
	auto it = this->_rooms.find(roomId);

	if (it == this->_rooms.end())
	{
		throw RoomException("Error: room '" + std::to_string(roomId) + "' was not found");
	}

	return it;
}