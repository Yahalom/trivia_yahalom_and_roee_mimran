#pragma once
#include "Constants.h"
#include "IDatabase.h"
#include "WSAInitializer.h"

class LoginManager
{
public:
	LoginManager(IDatabase* database);

	void signup(const User& user, SOCKET clientSocket);
	void login(const std::string& username, const std::string& password, SOCKET clientSocket);
	void logout(const std::string& username);

private:
	IDatabase* _database;
	std::vector<LoggedUser> _loggedUsers;

	std::vector<LoggedUser>::iterator findLoggedUser(const std::string& username);
};

