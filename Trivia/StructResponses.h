#pragma once
#include <vector>
#include "IDatabase.h"
#include "Room.h"

struct LoginResponse
{
	unsigned int status;

};

struct SignupResponse
{
	unsigned int status;

};

struct ErrorResponse
{
	std::string message;

};

struct LogoutResponse
{
	unsigned int status;
};

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
};

struct getStatisticsResponse
{
	unsigned int status;
	userStatistics statistics;
};

struct TopScoreResponse
{
	user_statistics_list topScores;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
	unsigned int roomId;
};

struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int timePerQuestion;
};

struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
};

struct LeaveGameResponse
{
	unsigned int status;
};

struct PlayerResult // Helper struct for "struct GetGameResultsResponse"
{
	std::string username;
	unsigned int correctAnswersCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
	int score;
};

struct GetGameResultsResponse
{
	unsigned int status;
	PlayerResult playerResult;

};

struct GetQuestionResponse
{
	std::string question;
};

struct GetAnswersResponse
{
	std::unordered_map<unsigned int, std::string> answers;
};