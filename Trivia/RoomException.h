#pragma once
#include "TriviaException.h"

class RoomException : public TriviaException
{
public:
	RoomException(const std::string& data) : TriviaException(data) {}
};
