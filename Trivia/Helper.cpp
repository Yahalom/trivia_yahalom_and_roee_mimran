#include "Helper.h"

/*
A method that converts a string in the format "['a', 'b', 'c']" to a vector.
Input:
	str - A string to convert.

Output:
	A converted string.
*/
std::vector<std::string> Helper::stringToVector(std::string str)
{
	std::vector<std::string> vectorStr;
	std::string part;

	clearStartAndEnd(str);
	std::istringstream iStr(str);

	while (std::getline(iStr, part, ','))
	{
		clearStartAndEnd(part);
		vectorStr.push_back(part);
	}

	return vectorStr;
}

float Helper::getAverageTime(const inGameUserStatistics& stats)
{
	return getAverageTime(stats.inGameCorrectAnswerCount + stats.inGameWrongAnswerCount, stats.totalInGameAnswerTime);
}

float Helper::getAverageTime(const GameData& data)
{
	return getAverageTime(data.wrongAnswerCount + data.correctAnswerCount, data.totalAnswerTime);
}

int Helper::getHighScore(const PlayerResult& result)
{
	return getHighScore(result.correctAnswersCount, result.averageAnswerTime);
}

float Helper::getAverageTime(const unsigned int numOfAnswers, const float totalTime)
{
	return totalTime / numOfAnswers;
}

int Helper::getHighScore(const unsigned int correctAnswers, const float averageTime)
{
	if (averageTime > 1)
	{
		return (correctAnswers * 100) * averageTime - (correctAnswers * 100);
	}
	else
	{
		return (correctAnswers * 100) + (correctAnswers * 100) * averageTime;
	}
}

/*
A method that removes all the spaces and one character from the start and end of a string.
Input:
	str - A string to clear.

Output:
	none.
*/
void Helper::clearStartAndEnd(std::string& str)
{
	if (str.size() > 1)
	{
		while (*str.begin() == ' ')
		{
			str.erase(str.begin());
		}

		while (*(str.end() - 1) == ' ')
		{
			str.erase(str.end());
		}

		str.erase(str.begin());
		str.erase(str.end() - 1);
	}
}