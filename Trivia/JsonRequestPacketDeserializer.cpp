#include "JsonRequestPacketDeserializer.h"
#include <bitset>

/*
function deserialize the login request from the client
input:
	the vector of bits from the socket

output:
	the login request struct
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(buffer bufferTodeserialize)
{
	LoginRequest loginRequest;

	json data = JsonRequestPacketDeserializer::extract(bufferTodeserialize);

	// extruct the values from there to the struct
	loginRequest.username = data.value("username", "");
	loginRequest.password = data.value("password", "");

	return loginRequest;
}

/*
function deserialize the signup request from the client
input:
	the vector of bits from the socket

output:
	the signup request struct
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(buffer bufferTodeserialize)
{
	SignupRequest signupRequest;

	json data = JsonRequestPacketDeserializer::extract(bufferTodeserialize);

	// extruct the values from there to the struct
	signupRequest.username = data.value("username", "");
	signupRequest.password = data.value("password", "");
	signupRequest.email = data.value("email", "");

	return signupRequest;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(buffer bufferTodeserialize)
{
	GetPlayersInRoomRequest request;
	
	json data = JsonRequestPacketDeserializer::extract(bufferTodeserialize);
	request.roomId = std::stoi(data.value("roomId", ""));

	return request;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(buffer bufferTodeserialize)
{
	JoinRoomRequest request;

	json data = JsonRequestPacketDeserializer::extract(bufferTodeserialize);
	request.roomId = std::stoi(data.value("roomId", ""));

	return request;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(buffer bufferTodeserialize)
{
	CreateRoomRequest request;

	json data = JsonRequestPacketDeserializer::extract(bufferTodeserialize);
	request.answerTimeout = std::stoi(data.value("answerTimeout", ""));
	request.maxUsers = std::stoi(data.value("maxUsers", ""));
	request.questionCount = std::stoi(data.value("questionCount", ""));
	request.roomName = data.value("roomName", "");

	return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(buffer bufferTodeserialize)
{
	SubmitAnswerRequest request;
	json data = JsonRequestPacketDeserializer::extract(bufferTodeserialize);

	request.chosenAnswer = std::stoi(data.value("chosenAnswer", ""));
	return request;
}

/*
A method that deserializes the data length of a message.
Input:
	buff - The buffer to deserialize from.

Output:
	The data length of a message.
*/
unsigned int JsonRequestPacketDeserializer::deserializeMessageDataLength(buffer buff)
{
	if (buff.size() < CODE_BYTE_SIZE + DATA_LEN_BYTE_SIZE)
	{
		throw DeserializerException("Error: buffer does not contain the data length");
	}

	std::string binaryDataLen;

	// converting the data length from the buffer to a binary string.
	for (int i = DATA_LEN_BYTE_SIZE; i >= CODE_BYTE_SIZE; i--)
	{
		binaryDataLen += std::bitset<BYTE_SIZE>(buff[i]).to_string();
	}

	return std::stoi(binaryDataLen, nullptr, 2); // converting the binary string to int.
}

json JsonRequestPacketDeserializer::extract(buffer buff)
{
	// removing the message code at the start of the buffer.
	buff.erase(buff.begin(), buff.begin() + 1);

	json data;

	try
	{
		// decode the vector into a json
		data = json::from_bson(buff);
	}
	catch (...)
	{
		throw DeserializerException("Error: data does not contain valid json");
	}

	return data;
}
