#pragma once
#include "Communicator.h"
#include "StructResponses.h"

class JsonResponsePacketSerializer
{
public:
	static buffer serializeResponse(struct LoginResponse loginRes);
	static buffer serializeResponse(struct SignupResponse signupRes);
	static buffer serializeResponse(struct ErrorResponse errorRes);

	static buffer serializeResponse(struct LogoutResponse logoutRes);
	static buffer serializeResponse(struct GetRoomsResponse getRoomRes);
	static buffer serializeResponse(struct GetPlayersInRoomResponse getPlayersRoomRes);
	static buffer serializeResponse(struct JoinRoomResponse joinRoomRes);
	static buffer serializeResponse(struct CreateRoomResponse createRoomRes);
	static buffer serializeResponse(struct getStatisticsResponse getStatisticsRes);
	static buffer serializeResponse(struct TopScoreResponse topScoreRes);

	static buffer serializeResponse(struct CloseRoomResponse closeRoomRes);
	static buffer serializeResponse(struct StartGameResponse startGameRes);
	static buffer serializeResponse(struct LeaveRoomResponse leaveRoomRes);
	static buffer serializeResponse(struct GetRoomStateResponse getRoomStateRes);

	static buffer serializeResponse(struct SubmitAnswerResponse submitAnswerRes);
	static buffer serializeResponse(struct LeaveGameResponse leaveGameRes);
	static buffer serializeResponse(struct GetGameResultsResponse getGameResultsRes);
	static buffer serializeResponse(struct GetQuestionResponse getQuestionsRes);

	static buffer serializeResponse(GetAnswersResponse GetAnswersRes);

private:
	static buffer getBuffer(unsigned int code, const json& data);
	static void insertByByteSize(buffer& buff, unsigned int num, unsigned int byteSize);

	static std::string join(const std::vector<std::string>& vector, std::string separator);

	static std::string join(const PlayerResult& result, std::string separator);

	static std::string serializeStatistics(struct userStatistics stats);

	static std::string join(const std::unordered_map<unsigned int, std::string> map, std::string separator);
};

