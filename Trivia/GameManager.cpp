#include "GameManager.h"

/*
A constructor for class GameManager.
Input:
	database - A database to use.

Output:
	none.
*/
GameManager::GameManager(IDatabase* database)
{
	this->_database = database;
}

/*
A method that creates a new game and adds it to the game vector.
Input:
	room - A player to get a question to.

Output:
	the game which created
*/
Game* GameManager::createGame(Room room)
{
	for (auto game : this->_games)
	{
		if (game->getName() == room.getData().name)
		{
			throw GameException("Error: a game with the name \"" + room.getData().name + "\" already exists");
		}
	}
	
	Game* game = new Game(room, this->_database->getQuestions(room.getData().questionCount));
	this->_games.push_back(game);
	return game;
}

/*
A method that removes a game from the game vector.
Input:
	name - The name of the game to remove.

Output:
	none.
*/
void GameManager::deleteGame(std::string name)
{
	for (auto it = this->_games.begin(); it != this->_games.end(); ++it)
	{
		if ((*it)->getName() == name)
		{ 
			delete* it;
			this->_games.erase(it);
			return;
		}
	}
}

/*
A method that get a game according to name
Input:
	name - The name of the game to get.

Output:
	the game.
*/
Game& GameManager::getGame(std::string name)
{
	for (auto it = this->_games.begin(); it != this->_games.end(); ++it)
	{
		if ((*it)->getName() == name)
		{
			return **it;
		}
	}
}
