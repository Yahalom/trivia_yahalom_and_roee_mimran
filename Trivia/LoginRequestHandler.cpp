#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handleFactory) : _handleFactory(handleFactory)
{
}

LoginRequestHandler::~LoginRequestHandler()
{
    delete this->_handleFactory;
}

/*
function check if thr request is relvant according to the id
input:
	the RequestInfo struct

output:
	if the request is relavent
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == MessageCodes::LoginRequestCode) ||
        (requestInfo.RequestId == MessageCodes::SignupRequestCode);
}

/*
function handle the request, create new ReuqestResult and fill him with the data
input:
	the RequestInfo that has the buffer and the id

output:
	the RequestResult struct fill with the buffer from the RequestInfo and the new handler
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo, SOCKET clientSocket)
{
	RequestResult requestResult;

    // checking if login or signup
    switch (requestInfo.RequestId)
    {
    case MessageCodes::LoginRequestCode:
        requestResult = this->login(JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer), clientSocket);
        break;

    case MessageCodes::SignupRequestCode:
        requestResult = this->signup(JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer), clientSocket);
        break;
    }
        
	return requestResult;
}

/*
function call to the login manager with the login and the user info
input: 
    the user info

output:
    the request result contain the 
*/
RequestResult LoginRequestHandler::login(LoginRequest request, SOCKET clientSocket)
{
    RequestResult result;
    
    // print the decoded data from client
    std::cout << std::endl << "login: " << std::endl
        << "    -password: " << request.password << std::endl
        << "    -username: " << request.username << std::endl;

    try
    {
        this->_handleFactory->getLoginManager().login(request.username, request.password, clientSocket);

        result.newHandler = this->_handleFactory->CreateMenuRequestHandler(request.username, clientSocket);
        
        // create succeed respone
        LoginResponse resp;
        resp.status = 1;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);

        return result;
    }
    catch (LoginManagerException& e)
    {
        result.newHandler = this->_handleFactory->createLoginRequestHandler();
    }
    catch (...)
    {
        result.newHandler = nullptr;
    }
    
    ErrorResponse resp;
    resp.message = "ERROR";
    result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);

    return result;
}

/*
function call to the login manager with the sign up and the user info
input:
    the user info

output:
    the request result contain the
*/
RequestResult LoginRequestHandler::signup(SignupRequest request, SOCKET clientSocket)
{
    RequestResult result;

    // print the decoded data from client
    std::cout << std::endl << "signup: " << std::endl
        << "    -password: " << request.password << std::endl
        << "    -username: " << request.username << std::endl
        << "    -email: " << request.email << std::endl;

    try
    {
        User newUser(request.username, request.password, request.email);
        this->_handleFactory->getLoginManager().signup(newUser, clientSocket);
        result.newHandler = this->_handleFactory->CreateMenuRequestHandler(newUser.getUsername(), clientSocket);

        // create succeed respone
        SignupResponse resp;
        resp.status = 1;
        result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);

        return result;
    }
    catch (LoginManagerException& e)
    {
        e.what();
        result.newHandler = this->_handleFactory->createLoginRequestHandler();
    }
    catch (...)
    {
        result.newHandler = nullptr;
    }

    ErrorResponse resp;
    resp.message = "ERROR";
    result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);

    return result;
}