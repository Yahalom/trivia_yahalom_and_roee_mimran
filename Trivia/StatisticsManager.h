#pragma once
#include "Constants.h"
#include "IDatabase.h"

class StatisticsManager
{
public:
	StatisticsManager(IDatabase* database);

	const userStatistics getUserStatistics(const std::string& username);
	const user_statistics_list getTopUsers(int numOfUsers);
	void updateUserStatistics(const inGameUserStatistics& gameStats);

private:
	IDatabase* _database;
};