#pragma once
#include "Constants.h"
#include "Question.h"

class IDatabase
{
public:
	virtual bool doesUserExist(const std::string& username) = 0;
	virtual bool doesPasswordMatch(const std::string& username, const std::string& password) = 0;

	virtual bool addNewUser(const User& user) = 0;

	virtual userStatistics getUserStatistics(const std::string& username) = 0;
	virtual user_statistics_list getTopUsers(unsigned int numOfusers) = 0;

	virtual questions getQuestions(unsigned int numOfQuestions) = 0;

	virtual void updateUserStatistics(const inGameUserStatistics& gameStats) = 0;
};

