#pragma once
#include <iostream>
#include <sstream>
#include <vector>
#include "Constants.h"
#include "StructResponses.h"

class Helper
{
public:
	/*
	A method that formats a string (code was taken from https://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf).
	Input:
		format - A string to format.
		args - Values of the format.
	
	Output:
		none.
	*/
	template<typename ... Args>
	static std::string format(const std::string& format, Args ... args)
	{
		size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'

		if (size <= 0)
		{
			throw std::runtime_error("Error during formatting.");
		}

		std::unique_ptr<char[]> buf(new char[size]);
		snprintf(buf.get(), size, format.c_str(), args ...);

		return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
	}

	static std::vector<std::string> stringToVector(std::string str);

	static float getAverageTime(const inGameUserStatistics& stats);
	static float getAverageTime(const GameData& result);
	static int getHighScore(const PlayerResult& result);
	
	static float getAverageTime(const unsigned int numOfAnswers, const float totalTime);
	static int getHighScore(const unsigned int correctAnswers, const float averageTime);

private:
	static void clearStartAndEnd(std::string& str);
};

