#pragma once
#include "TriviaException.h"

class MessageException : public TriviaException
{
public:
	MessageException(const std::string& data) : TriviaException(data) {}
};