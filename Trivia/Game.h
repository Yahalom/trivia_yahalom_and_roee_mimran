#pragma once
#include "Constants.h"
#include "Question.h"
#include "Room.h"
#include <mutex>

class Game
{
public:
	Game(const Room& gameRoom, const questions& gameQuestions);

	const std::string getName();
	const inGamePlayers getInGamePlayers();
	const Question getQuestionForUser(const LoggedUser& player);

	unsigned int submitAnswer(const LoggedUser& submitter, unsigned int answerId);

	void removePlayer(const LoggedUser& player);
	void leaveGame(const LoggedUser& player);

	int countOnlinePlayers();

	bool updateStatistics();

	void waitForEndGame();

	const int getEndGamePlayersCounter();
	const int getPlayersCount();

private:
	int _endGamePlayersCounter;
	std::string _name;
	questions _questions;
	inGamePlayers _players;
	bool _wereStatsUpdated;

	std::mutex _questionMutex;
	std::unique_lock<std::mutex> _questionULock;

	std::mutex _playersMutex;
	std::unique_lock<std::mutex> _playersULock;

	std::mutex _gameMutex;
	std::condition_variable _gameCondition;

	std::mutex _endGameCounterMutex;
	std::unique_lock<std::mutex> _endgameCounterULock;

	std::mutex _updateStatsMutex;
	std::unique_lock<std::mutex> _updateStatsULock;

	void lockStatisticsUpdating();
	void unlockStatisticsUpdating();

	void lockQuestion();
	void unlockQuestion();

	void lockPlayers();
	void unlockPlayers();

	void lockEndGameCounter();
	void unlockEndGameCounter();

	void waitForEnd();
	void EndGame();

	inGamePlayers::iterator findPlayer(const LoggedUser& player);
	Question getCurrentQuestion(const inGamePlayer& player);
};

typedef std::vector<Game*> games;