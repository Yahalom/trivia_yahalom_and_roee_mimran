#include "JsonResponsePacketSerializer.h"
#include <bitset>
#include <math.h>

/*
A method that converts a login response to a buffer according to the protocol.
Input:
	loginRes - The login response to convert.

Output:
	A login response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct LoginResponse loginRes)
{
	json loginResponseJson = { {"status", loginRes.status} };

	return getBuffer(MessageCodes::LoginResponseCode, loginResponseJson);
}

/*
A method that converts a signup response to a buffer according to the protocol.
Input:
	signupRes - The signup response to convert.

Output:
	A signup response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct SignupResponse signupRes)
{
	json signupResponseJson = { {"status", signupRes.status} };

	return getBuffer(MessageCodes::SignupResponseCode, signupResponseJson);
}

/*
A method that converts an error response to a buffer according to the protocol.
Input:
	errorRes - The error response to convert.

Output:
	An error response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct ErrorResponse errorRes)
{
	json errorResponseJson = { {"message", errorRes.message} };

	return getBuffer(MessageCodes::ErrorResponseCode, errorResponseJson);
}

/*
A method that converts a logout response to a buffer according to the protocol.
Input:
	logoutRes - The Logout response to convert.

Output:
	A Logout response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct LogoutResponse logoutRes)
{
	json logoutResponseJson = { {"status", logoutRes.status} };

	return getBuffer(MessageCodes::LogoutResponseCode, logoutResponseJson);
}

/*
A method that converts a GetRooms response to a buffer according to the protocol.
Input:
	getRoomRes - The GetRooms response to convert.

Output:
	A GetRooms response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct GetRoomsResponse getRoomRes)
{
	std::string rooms;
	std::vector<std::string> roomsNames;

	// create vector of the names of the rooms
	for (const auto& room : getRoomRes.rooms)
	{
		roomsNames.push_back(room.name + ":" + std::to_string(room.id));
	}
	
	json getRoomsJson = { {"status", getRoomRes.status}, {"rooms", JsonResponsePacketSerializer::join(roomsNames, ",") } };

	return getBuffer(MessageCodes::GetRoomsResponseCode, getRoomsJson);
}

/*
A method that converts a GetPlayersInRoom response to a buffer according to the protocol.
Input:
	getPlayersRoomRes - The GetPlayersInRoom response to convert.

Output:
	A GetPlayersInRoom response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct GetPlayersInRoomResponse getPlayersRoomRes)
{
	json GetPlayersInRoomResponseJson = { {"PlayersInRoom",JsonResponsePacketSerializer::join(getPlayersRoomRes.players, ",")} };

	return getBuffer(MessageCodes::GetPlayersInRoomResponseCode, GetPlayersInRoomResponseJson);
}

/*
A method that converts a JoinRoom response to a buffer according to the protocol.
Input:
	joinRoomRes - The JoinRoom response to convert.

Output:
	A JoinRoom response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct JoinRoomResponse joinRoomRes)
{
	json JoinRoomResponseJson = { {"status", joinRoomRes.status} };

	return getBuffer(MessageCodes::JoinRoomResponseCode, JoinRoomResponseJson);
}

/*
A method that converts a CreateRoom response to a buffer according to the protocol.
Input:
	createRoomRes - The CreateRoom response to convert.

Output:
	A CreateRoom response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct CreateRoomResponse createRoomRes)
{
	json CreateRoomResponseJson = { {"status", createRoomRes.status}, { "roomId", createRoomRes.roomId } };

	return getBuffer(MessageCodes::CreateRoomResponseCode, CreateRoomResponseJson);
}

/*
A method that converts a getStatistics response to a buffer according to the protocol.
Input:
	getStatisticsRes - The getStatistics response to convert.

Output:
	A getStatistics response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct getStatisticsResponse getStatisticsRes)
{
	json getStatisticsJson = { { "statistics", JsonResponsePacketSerializer::serializeStatistics(getStatisticsRes.statistics) + "," + std::to_string(getStatisticsRes.status) } };
	return getBuffer(MessageCodes::GetStatisticsResponseCode, getStatisticsJson);
}


/*
A method that converts a closeRoom response to a buffer according to the protocol.
Input:
	closeRoomRes - The closeRoom response to convert.

Output:
	A closeRoom response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct CloseRoomResponse closeRoomRes)
{
	json closeRoomResponseJson = { {"status", closeRoomRes.status} };

	return getBuffer(MessageCodes::CloseRoomResponseCode, closeRoomResponseJson);
}

buffer JsonResponsePacketSerializer::serializeResponse(struct TopScoreResponse topScoreRes)
{
	std::string statistics;
	for (auto topScore : topScoreRes.topScores)
	{
		statistics += JsonResponsePacketSerializer::serializeStatistics(topScore) + "&";
	}

	// remove the & at the end
	statistics.pop_back();
	
	json topScoreJson = { { "topScore", statistics } };
	return getBuffer(MessageCodes::GetTopScoreResponseCode, topScoreJson);
}



/*
A method that converts a startGame response to a buffer according to the protocol.
Input:
	startGameRes - The startGame response to convert.

Output:
	A startGame response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct StartGameResponse startGameRes)
{
	json startGameResponseJson = { {"status", startGameRes.status} };

	return getBuffer(MessageCodes::StartGameResponseCode, startGameResponseJson);
}

/*
A method that converts a leaveRoom response to a buffer according to the protocol.
Input:
	leaveRoomRes - The leaveRoom response to convert.

Output:
	A leaveRoom response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct LeaveRoomResponse leaveRoomRes)
{
	json leaveRoomResponseJson = { {"status", leaveRoomRes.status} };

	return getBuffer(MessageCodes::leaveRoomResponseCode, leaveRoomResponseJson);
}

/*
A method that converts a getRoomState response to a buffer according to the protocol.
Input:
	getRoomStateRes - The getRoomState response to convert.

Output:
	A getRoomState response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct GetRoomStateResponse getRoomStateRes)
{
	json getRoomStateResponseJson = { {"status", getRoomStateRes.status},
		{"hasGameBegun", getRoomStateRes.hasGameBegun},
		{"players", JsonResponsePacketSerializer::join(getRoomStateRes.players, ",")},
		{"questionCount", getRoomStateRes.questionCount},
		{"timePerQuestion", getRoomStateRes.timePerQuestion} };

	return getBuffer(MessageCodes::GetRoomStateResponseCode, getRoomStateResponseJson);
}

/*
A method that converts a submitAnswer response to a buffer according to the protocol.
Input:
	submitAnswerRes - The submitAnswer response to convert.

Output:
	A submitAnswer response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct SubmitAnswerResponse submitAnswerRes)
{
	json submitAnswerResponseJson = { {"status", submitAnswerRes.status},
		{"correctAnswerId", submitAnswerRes.correctAnswerId} };

	return getBuffer(MessageCodes::SubmitAnswerResponseCode, submitAnswerResponseJson);
}

/*
A method that converts a leaveGame response to a buffer according to the protocol.
Input:
	leaveGameRes - The leaveGame response to convert.

Output:
	A leaveGame response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct LeaveGameResponse leaveGameRes)
{
	json leaveGameResponseJson = { {"status", leaveGameRes.status} };

	return getBuffer(MessageCodes::leaveRoomResponseCode, leaveGameResponseJson);
}

/*
A method that converts a getGameResults response to a buffer according to the protocol.
Input:
	getGameResultsRes - The getGameResults response to convert.

Output:
	A getGameResults response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct GetGameResultsResponse getGameResultsRes)
{
	json getGameResultsResponseJson = { {"playerResults", JsonResponsePacketSerializer::join(getGameResultsRes.playerResult, ",")} };

	return getBuffer(MessageCodes::GetGameResultsResponseCode, getGameResultsResponseJson);
}

/*
A method that converts a getQuestions response to a buffer according to the protocol.
Input:
	getQuestionsRes - The getQuestions response to convert.

Output:
	A getQuestions response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct GetQuestionResponse getQuestionRes)
{
	json getQuestionsResponseJson = { { "question", getQuestionRes.question } };

	return getBuffer(MessageCodes::GetQuestionResponseCode, getQuestionsResponseJson);
}

/*
A method that converts a getQuestions response to a buffer according to the protocol.
Input:
	getQuestionsRes - The getQuestions response to convert.

Output:
	A getQuestions response buffer.
*/
buffer JsonResponsePacketSerializer::serializeResponse(struct GetAnswersResponse GetAnswersRes)
{
	json getQuestionsResponseJson = { { "answers", JsonResponsePacketSerializer::join(GetAnswersRes.answers, ",") } };

	return getBuffer(MessageCodes::GetQuestionResponseCode, getQuestionsResponseJson);
}


/*
A method that creates a buffer according to the protocol.
Input:
	code - The message code of the buffer.
	data - The data of the buffer in a json format.

Output:
	A buffer.
*/
buffer JsonResponsePacketSerializer::getBuffer(unsigned int code, const json& data)
{
	buffer buff;
	buffer binaryJson = json::to_bson(data); // converting the data into binary.

	// creating the buffer (code + data length + data)
	insertByByteSize(buff, code, CODE_BYTE_SIZE);
	buff.insert(buff.end(), binaryJson.begin(), binaryJson.end()); // the first four bytes of the binary json containes its length.

	return buff;
}

/*
A method that inserts a number into specific number of bytes of a buffer.
Input:
	buff - A buffer to insert into.
	num - A number to insert.
	byteSize - The number of bytes to insert into (have to be smaller or equal to the byte size of "num" (currently int)).

Output:
	none.
*/
void JsonResponsePacketSerializer::insertByByteSize(buffer& buff, unsigned int num, unsigned int byteSize)
{
	if (byteSize > sizeof(num)) // checking if the given byte size is in the max byte size limit (the number of bytes of the numbers's type (currently int)).
	{
		throw SerializerException("Error: given bite size is bigger than the maximum");
	}

	if (num > pow(2, byteSize * BYTE_SIZE)) // checking if the number is in the given byte limit.
	{
		throw SerializerException("Error: the given number does not fit to its byte size");
	}

	std::string binaryStr = std::bitset<sizeof(num) * BYTE_SIZE>(num).to_string(); // conzerting the number to binary in the max byte size (the number of bytes of the numbers's type (currently int)).

	for (unsigned int i = (sizeof(num) - byteSize) * BYTE_SIZE; i < sizeof(num) * BYTE_SIZE; i += BYTE_SIZE) // looping on every byte (substring of 8 chars) and adding it to the buffer (at the start if the max byte size is bigger from the given byte size skiping the unwanted bytes). 
	{
		buff.push_back(std::stoi(binaryStr.substr(i, BYTE_SIZE), nullptr, 2));
	}
}

std::string JsonResponsePacketSerializer::join(const std::vector<std::string>& vector, std::string separator)
{
	std::string final = "[";

	// take all the rooms to one string seperated with coma
	for (auto& part : vector)
	{
		final += part + separator;
	}

	// removing the last ',' if the string is not empty (ignoring the '[').
	if (final.size() > 1)
	{
		final.pop_back();
	}

	final += "]";

	return final;
}

std::string JsonResponsePacketSerializer::join(const PlayerResult& result, std::string separator)
{
	std::string final = "[";

	final += "username:" + result.username + separator;
	final += "correctAnswersCount:" + std::to_string(result.correctAnswersCount) + separator;
	final += "wrongAnswerCount:" + std::to_string(result.wrongAnswerCount) + separator;
	final += "averageAnswerTime:" + std::to_string(result.averageAnswerTime) + separator;
	final += "score:" + std::to_string(result.score);

	final += "]";

	return final;
}

std::string JsonResponsePacketSerializer::join(const std::unordered_map<unsigned int, std::string> map, std::string separator)
{
	std::string final = "[";

	// take all the rooms to one string seperated with coma
	for (auto& part : map)
	{
		final += part.second + separator;
	}

	// removing the last ',' if the string is not empty (ignoring the '[').
	if (final.size() > 1)
	{
		final.pop_back();
	}

	final += "]";

	return final;
}


std::string JsonResponsePacketSerializer::serializeStatistics(userStatistics stats)
{
	std::string statistics = std::to_string(stats.highScore) + "," +
		std::to_string(stats.numOfAnswers) + "," +
		std::to_string(stats.numOfCorrectAnswers) + "," +
		std::to_string(stats.numOfGames) + "," +
		std::to_string(stats.numOfGamesWon) + "," +
		std::to_string(stats.totalAnswerTime) + "," +
		stats.username;

	return statistics;
}
