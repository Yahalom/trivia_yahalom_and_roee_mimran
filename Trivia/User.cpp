#include "User.h"

User::User(const std::string& username, const std::string& password, const std::string& email)
{
	this->_username = username;
	this->_password = password;
	this->_email = email;
}

std::string User::getUsername() const
{
	return this->_username;
}

std::string User::getPassword() const
{
	return this->_password;
}

std::string User::getEmail() const
{
	return this->_email;
}
