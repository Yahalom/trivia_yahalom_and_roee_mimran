#pragma once
#include "TriviaException.h"

class SerializerException : public TriviaException
{
public:
	SerializerException(const std::string& data) : TriviaException(data) {}
};