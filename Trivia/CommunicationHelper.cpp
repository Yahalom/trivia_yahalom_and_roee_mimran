#include "CommunicationHelper.h"


/*
A method that sends a string to a connected socket.
Input:
	destination - A socket to send to.
	data - The data to send.

Output:
	none.
*/
void CommunicationHelper::sendString(SOCKET destination, const std::string& data)
{
	if (send(destination, data.c_str(), data.size(), 0) == INVALID_SOCKET)
	{
		throw SocketException("Error: failed to send data");
	}
}

/*
A method that waits and gets a string from a connected socket.
Input:
	source - A socket to get data from.
	maxSize - The maximum size of the received data.

Output:
	The string that was received from the socket.
*/
std::string CommunicationHelper::receiveString(SOCKET source, int maxSize)
{
	char* data = new char[maxSize];

	if (recv(source, data, maxSize, 0) == INVALID_SOCKET)
	{
		throw SocketException("Error: failed to get data");
	}

	std::string strData;

	for (int i = 0; i < maxSize; i++)
	{
		strData += data[i];
	}

	delete[] data;

	return strData;
}

/*
A method that sends a buffer to a connected socket.
Input:
	destination - A socket to send to.
	data - The data to send.

Output:
	none.
*/
void CommunicationHelper::sendBuffer(SOCKET destination, const buffer& data)
{
	std::string bufferStr;
	// coping the buffer into a string.
	for (auto it = data.begin(); it != data.end(); ++it)
	{
		bufferStr += *it;
	}

	sendString(destination, bufferStr);
}

/*
A method that waits and gets a buffer from a connected socket.
Input:
	source - A socket to get data from.
	maxSize - The maximum size of the received data.

Output:
	The buffer that was received from the socket.
*/
buffer CommunicationHelper::receiveBuffer(SOCKET source, int maxSize)
{
	std::string bufferStr;

	if (maxSize > 0)
	{
		bufferStr = receiveString(source, maxSize);
	}

	buffer data;

	for (int i = 0; i < maxSize; i++)
	{
		data.push_back(bufferStr[i]);
	}

	return data;
}

/*
function send to a vector of sockets the data
input:
	users - vector of sockets to send
	buffer - the data to send

output:
	none
*/
void CommunicationHelper::sendBufferToSomeUsers(std::vector<SOCKET> users, const buffer& data)
{
	for (auto& user : users)
	{
		CommunicationHelper::sendBuffer(user, data);
	}
}