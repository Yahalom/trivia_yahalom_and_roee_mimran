#pragma once
#include "Constants.h"

struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int numOfQuestions;
	bool isActive;
	unsigned int questionCount;
};

class Room
{
public:
	Room(const RoomData& data);

	void addUser(const LoggedUser& user);
	void removeUser(const LoggedUser& user);
	const std::vector<LoggedUser>& getAllUsers() const;
	const RoomData& getData() const;
	void close();

private:
	RoomData _metadata;
	std::vector<LoggedUser> _inRoomUsers;

	void updateStatus();
};

