#pragma once
#include <string>

class User
{
public:
	User(const std::string& username, const std::string& password, const std::string& email);

	std::string getUsername() const;
	std::string getPassword() const;
	std::string getEmail() const;

private:
	std::string _username;
	std::string _password;
	std::string _email;
};

