#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser loggedUser, RoomManager* roomManager, RequestHandlerFactory* handlerFactory, Room* room) :
	_room(room), _user(loggedUser), _handlerFactory(handlerFactory), _roomManager(roomManager)
{
	this->_memberAction = this->_handlerFactory->createRoomMemberRequestHandler(loggedUser.getUsername(), loggedUser.getSocket(), room);
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == MessageCodes::CloseRoomRequestCode) ||
		(requestInfo.RequestId == MessageCodes::StartGameRequestCode) ||
		this->_memberAction->isRequestRelevant(requestInfo);
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo, SOCKET clientSocket)
{
	RequestResult result;

	try
	{
		switch (requestInfo.RequestId)
		{
		case MessageCodes::CloseRoomRequestCode:
			result = this->closeRoom(requestInfo);
			break;

		case MessageCodes::StartGameRequestCode:
			result = this->startGame(requestInfo);
			break;

		default:
			result = this->_memberAction->handleRequest(requestInfo, clientSocket);
			delete result.newHandler;
			result.newHandler = this->_handlerFactory->createRoomAdminRequestHandler(this->_user.getUsername(), this->_user.getSocket(), this->_room);
			break;
		}

	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
		ErrorResponse resp;
		resp.message = "ERROR";
		result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);
		result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());
	}
	return result;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo)
{
	LeaveRoomResponse response;
	RequestResult result;
	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);

	// send all the users except this user, this msg will be sent after the handle request at the main
	CommunicationHelper::sendBufferToSomeUsers(this->getAllSocketsExeptMine(), result.buffer);
	
	// create a room member so he will ba able to leave the room
	result.newHandler = this->_handlerFactory->createRoomMemberRequestHandler(this->_user.getUsername(), this->_user.getSocket(), this->_room);
	return result;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo)
{
	StartGameResponse response;
	RequestResult result;

	// creating the game and closing the room.
	Game* game = this->_handlerFactory->getGameManager().createGame(*this->_room);
	this->_room->close();

	response.status = 1;
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);

	// change to game request handler
	result.newHandler = this->_handlerFactory->createGameRequestHandler(this->_user, game, this->_room->getData().id);
	return result;
}

/*
function get a vector of sockets of the room except this current user
*/
std::vector<SOCKET> RoomAdminRequestHandler::getAllSocketsExeptMine()
{
	std::vector<SOCKET> sockets;
	std::vector<LoggedUser> users = this->_room->getAllUsers();

	for (LoggedUser user : users)
	{
		if (this->_user.getUsername() != user.getUsername())
		{
			sockets.push_back(user.getSocket());
		}
	}

	return sockets;
}
