#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser loggedUser, RoomManager* roomManager, RequestHandlerFactory* handlerFactory, StatisticsManager* statisticsManager) :
	_user(loggedUser), _roomManager(roomManager), _handlerFactory(handlerFactory), _statisticsManager(statisticsManager)
{
}

/*
function check if the request is relevant to the menu request handler
input:
	the request info contain the request id

output:
	relevant or not
*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == MessageCodes::CreateRoomRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetPlayersInRoomRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetRoomsRequestCode) ||
		(requestInfo.RequestId == MessageCodes::JoinRoomRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetStatisticsRequestCode) ||
		(requestInfo.RequestId == MessageCodes::LogoutRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetTopScoreRequestCode);
}

/*
function handle the request, send the request info to the right function
input:
	the request info contain the buffer and the id

output:
	the request result contain the buffer to send
*/
RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo, SOCKET clientSocket)
{
	RequestResult result;

	try
	{
		switch (requestInfo.RequestId)
		{
		case MessageCodes::LogoutRequestCode:
			result = this->signout(requestInfo);
			break;

		case MessageCodes::CreateRoomRequestCode:
			result = this->createRoom(requestInfo);
			break;

		case MessageCodes::GetPlayersInRoomRequestCode:
			result = this->getPlayersInRoom(requestInfo);
			break;

		case MessageCodes::GetRoomsRequestCode:
			result = this->getRooms(requestInfo);
			break;

		case MessageCodes::JoinRoomRequestCode:
			result = this->joinRoom(requestInfo);
			break;

		case MessageCodes::GetStatisticsRequestCode:
			result = this->getStatistics(requestInfo);
			break;

		case MessageCodes::GetTopScoreRequestCode:
			result = this->getTopScore(requestInfo);
			break;		
		}
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
		ErrorResponse resp;
		resp.message = "ERROR";
		result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);
		result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());
		return result;
	}

	return result;
}

/*
function look to remove a user from the client
input:
	the requst of the user contain the buffer

output
:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::signout(RequestInfo request)
{
	RequestResult result;
	LogoutResponse response;

	// look if the client is in a room
	for (auto& room : this->_roomManager->getRooms())
	{
		// checking if the user in it
		try
		{
			this->_roomManager->getRoom(room.id).removeUser(this->_user);

			// if the code came here, non throw was send and the user got removed from the room
			break;
		}
		catch (...)
		{
		}
	}

	response.status = 1;
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);

	this->_handlerFactory->getLoginManager().logout(this->_user.getUsername());

	result.newHandler = this->_handlerFactory->createLoginRequestHandler();

	return result;
}

/*
function send the all the rooms to the user
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo request)
{
	GetRoomsResponse response;
	RequestResult result;

	response.rooms = this->_roomManager->getRooms();
	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());

	return result;
}

/*
function get all the players in the room
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo request)
{
	GetPlayersInRoomRequest getPlayersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request.buffer);
	RequestResult result;
	GetPlayersInRoomResponse response;
	
	// get all the names of the users in the room
	for (auto& user : this->_roomManager->getRoom(getPlayersInRoomRequest.roomId).getAllUsers())
	{
		// push the name to the response
		response.players.push_back(user.getUsername());
	}
	
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());

	return result;
}

/*
function get the Statistics of a user
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::getStatistics(RequestInfo request)
{
	getStatisticsResponse response;
	response.statistics = this->_statisticsManager->getUserStatistics(this->_user.getUsername());
	response.status = 1;
	
	RequestResult result;
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);

	result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());
	return result;
}

/*
function join a room
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo request)
{
	JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request.buffer);
	RequestResult result;

	this->_roomManager->getRoom(joinRoomRequest.roomId).addUser(this->_user);


	JoinRoomResponse response;
	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);

	result.newHandler = this->_handlerFactory->createRoomMemberRequestHandler(this->_user.getUsername(), this->_user.getSocket(), &this->_roomManager->getRoom(joinRoomRequest.roomId));
	return result;
}

/*
function create a room
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo request)
{
	CreateRoomRequest createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request.buffer);
	RequestResult result;
	CreateRoomResponse response;

	// create the room data
	RoomData roomData;
	roomData.maxPlayers = createRoomRequest.maxUsers;
	roomData.name = createRoomRequest.roomName;
	roomData.timePerQuestion = createRoomRequest.answerTimeout;
	roomData.questionCount = createRoomRequest.questionCount;
	roomData.isActive = false;
		
	response.roomId = this->_roomManager->createRoom(this->_user, roomData);
	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);	
	
	result.newHandler = this->_handlerFactory->createRoomAdminRequestHandler(this->_user.getUsername(), this->_user.getSocket(), &this->_handlerFactory->getRoomManager().getRoom(response.roomId));
	return result;
	
}

/*
function get the top scores according to NUM_OF_USERS_IN_TOP_SCORE
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult MenuRequestHandler::getTopScore(RequestInfo request)
{
	TopScoreResponse response;
	RequestResult result;

	response.topScores = this->_statisticsManager->getTopUsers(NUM_OF_USERS_IN_TOP_SCORE);
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);

	result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());
	return result;
}
