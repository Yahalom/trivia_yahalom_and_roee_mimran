#pragma once

#include "LoggedUser.h"
#include "Room.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "CommunicationHelper.h"
#include "Constants.h"
#include "RoomMemberRequestHandler.h"

class RequestHandlerFactory;
class RoomMemberRequestHandler;

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser loggedUser, RoomManager* roomManager, RequestHandlerFactory* handlerFactory, Room* room);

	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo, SOCKET clientSocket);

private:
	LoggedUser _user;
	Room* _room;
	RoomManager* _roomManager;
	RequestHandlerFactory* _handlerFactory;
	RoomMemberRequestHandler* _memberAction;

	RequestResult closeRoom(RequestInfo);
	RequestResult startGame(RequestInfo);

	std::vector<SOCKET> getAllSocketsExeptMine();
};

