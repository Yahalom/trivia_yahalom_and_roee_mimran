#pragma once
#include "TriviaException.h"

class DeserializerException : public TriviaException
{
public:
	DeserializerException(const std::string& data) : TriviaException(data) {}
};