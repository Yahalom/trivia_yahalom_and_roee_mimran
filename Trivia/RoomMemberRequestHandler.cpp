#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser loggedUser, RoomManager* roomManager, RequestHandlerFactory* handlerFactory, Room* room) :
	_room(room), _user(loggedUser), _handlerFactory(handlerFactory), _roomManager(roomManager)
{
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == MessageCodes::leaveRoomRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetRoomStateRequestCode) ||
		(requestInfo.RequestId == MessageCodes::StartGameRequestCode);
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo, SOCKET socket)
{
	RequestResult result;

	try
	{
		switch (requestInfo.RequestId)
		{
		case MessageCodes::leaveRoomRequestCode:
			result = this->leaveRoom(requestInfo);
			break;

		case MessageCodes::GetRoomStateRequestCode:
			result = this->getRoomState(requestInfo);
			break;

		case MessageCodes::StartGameRequestCode:
			result = this->startGame(requestInfo);
			break;
		}
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
		ErrorResponse resp;
		resp.message = "ERROR";
		result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);
		result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());
	}

	return result;
}


RequestResult RoomMemberRequestHandler::startGame(RequestInfo requestInfo)
{
	RequestResult result;

	StartGameResponse response;
	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_handlerFactory->createGameRequestHandler(this->_user, &this->_handlerFactory->getGameManager().getGame(this->_room->getData().name), this->_room->getData().id);
	
	return result;
}


RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo requestInfo)
{
	LeaveRoomResponse response;
	RequestResult result;

	this->_room->removeUser(this->_user);
		
	response.status = 1;
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_handlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());

	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo requestInfo)
{
	GetRoomStateResponse response;
	RequestResult result;

	for (const auto& user : this->_room->getAllUsers())
	{
		response.players.push_back(user.getUsername());
	}

	auto& roomData = this->_room->getData();
	response.timePerQuestion = roomData.timePerQuestion;
	response.hasGameBegun = !roomData.isActive;
	response.questionCount = roomData.questionCount;
	response.status = 1;


	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_handlerFactory->createRoomMemberRequestHandler(this->_user.getUsername(), this->_user.getSocket(), this->_room);

	return result;
}