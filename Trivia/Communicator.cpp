#include "Communicator.h"

/*
A constructor for class Communicator.
Input:
	none.

Output:
	none.
*/
Communicator::Communicator(RequestHandlerFactory* factory) : _factory(factory)
{
	this->_serverSocket = NULL;
}

/*
A destructor for class Communicator.
Input:
	none.

Output:
	none.
*/
Communicator::~Communicator()
{
	closesocket(this->_serverSocket);
}

/*
A method that listens and accepts clients to the client map.
Input:
	none.

Output:
	none.
*/
void Communicator::startHandleRequests()
{
	this->bindAndListen();

	SOCKET newClientSocket;

	while (true)
	{
		newClientSocket = accept(this->_serverSocket, NULL, NULL);

		if (newClientSocket != INVALID_SOCKET)
		{
			this->_clients.insert(newClientSocket, this->_factory->createLoginRequestHandler(), new std::thread(&Communicator::handleNewClient, this, newClientSocket)); // inserting a new client to the client map with a login state.
		}
	}
}

/*
A method that binds the server's socket and starts to listen.
Input:
	none.

Output:
	none.
*/
void Communicator::bindAndListen()
{
	struct sockaddr_in sockAddr = { 0 };

	// binding:
	sockAddr.sin_port = htons(SERVER_PORT);
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = INADDR_ANY;

	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_serverSocket == INVALID_SOCKET)
	{
		throw SocketException("Error: invalid server socket");
	}

	if (bind(this->_serverSocket, (struct sockaddr*) & sockAddr, sizeof(sockAddr)) == SOCKET_ERROR)
	{
		throw SocketException("Error: failed to bind server socket");
	}

	// listening:
	if (listen(this->_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw SocketException("Error: failed to listen on server socket");
	}
}

/*
A method that handles a connection with a client.
Input:
	clientSocket - The socket of the client to be handled.

Output:
	none.
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		IRequestHandler* requestHandler = this->_factory->createLoginRequestHandler();
		RequestResult result;
		RequestInfo request;

		while (requestHandler != nullptr)
		{
			request = this->receiveMessage(clientSocket);

			// check if the message is relevant
			if (requestHandler->isRequestRelevant(request))
			{
				// handle the request
				result = requestHandler->handleRequest(request, clientSocket);
				CommunicationHelper::sendBuffer(clientSocket, result.buffer);				

				// delete the last handler and update the next one
				delete requestHandler;
				requestHandler = result.newHandler;
			}
			else
			{
				std::cout << "Message not relevant" << std::endl;
			}
		}
	}
	catch (TriviaException & ex)
	{
		ex.what();
	}

	this->_clients.remove(clientSocket);
}

/*
A method that waits and gets a message from a connected socket.
Input:
	source - A socket to get data from.
	maxSize - The maximum size of the received data.

Output:
	The message buffer that was received from the socket.
*/
RequestInfo Communicator::receiveMessage(SOCKET source)
{
	RequestInfo request;

	buffer buff = CommunicationHelper::receiveBuffer(source, CODE_BYTE_SIZE + DATA_LEN_BYTE_SIZE); // getting the header of the message
	buffer data = CommunicationHelper::receiveBuffer(source, JsonRequestPacketDeserializer::deserializeMessageDataLength(buff)); // getting the data of the message.

	buff.insert(buff.end(), data.begin(), data.end()); // conecting the header and data into one buffer.

	buff.erase(std::remove(buff.begin(), buff.end(), RECV_END), buff.end()); // remove non-message parts of the buffer.

	request.RequestId = buff.front();
	request.buffer = buff;

	return request;
}