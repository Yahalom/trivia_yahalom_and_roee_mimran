#include "StatisticsManager.h"

/*
A constructor for class StatisticsManager.
Input:
	database - The database type to use.

Output:
	none.
*/
StatisticsManager::StatisticsManager(IDatabase* database)
{
	this->_database = database;
}

/*
A method that returns all the statistics of a user.
Input:
	username - The name of the user to get the statistics from.

Output:
	All the statistics of a user.
*/
const userStatistics StatisticsManager::getUserStatistics(const std::string& username)
{
	return this->_database->getUserStatistics(username);
}

/*
A method that returns all the statistics of the top users.
Input:
	numOfusers - The number of top users to be returneds.

Output:
	All the statistics of the top users.
*/
const user_statistics_list StatisticsManager::getTopUsers(int numOfUsers)
{
	return this->_database->getTopUsers(numOfUsers);
}

void StatisticsManager::updateUserStatistics(const inGameUserStatistics& gameStats)
{
	this->_database->updateUserStatistics(gameStats);
}
