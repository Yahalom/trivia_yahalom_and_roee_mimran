#pragma once
#include "Constants.h"
#include "IDatabase.h"
#include "Game.h"

class GameManager
{
public:
	GameManager(IDatabase* database);

	Game* createGame(Room room);
	void deleteGame(std::string name);

	Game& getGame(std::string name);

private:
	IDatabase* _database;
	games _games;
};

