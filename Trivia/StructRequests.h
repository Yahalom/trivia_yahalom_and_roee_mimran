#pragma once

#include <string>
#include <time.h>
#include <vector>
#include "IRequestHandler.h"

// prevent circular dependency from IRequestHandler.h
class IRequestHandler;

// all the requests from the client 

struct RequestResult
{
	std::vector<unsigned char> buffer;
	IRequestHandler* newHandler;
};

struct RequestInfo
{
	int RequestId;
	time_t receivalTime;
	std::vector<unsigned char> buffer;
};

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct SubmitAnswerRequest
{
	unsigned int chosenAnswer;
};