#pragma once
#include "TriviaException.h"

class SocketException : public TriviaException
{
public:
	SocketException(const std::string& data) : TriviaException(data) {}
};