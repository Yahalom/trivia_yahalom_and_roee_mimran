#pragma once
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "StructRequests.h"
#include "Game.h"
#include <mutex>

struct PlayerResult;

// early declartion in order to prevent circualr dependency
class RequestHandlerFactory;
class GameManager;
class Game;

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(LoggedUser user, GameManager* gameManager, RequestHandlerFactory* factory, Game* game, int roomId);

	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo, SOCKET clientSocket);

private:
	Game* _game;
	LoggedUser _user;
	GameManager* _gameManager;
	RequestHandlerFactory* _requestHandlerFactory;
	int _gameId;

	RequestResult getQuestion(RequestInfo, SOCKET clientSocket);
	RequestResult submitAnswer(RequestInfo);
	RequestResult getGameResults(RequestInfo, SOCKET clientSocket);
	RequestResult leaveGame(RequestInfo);
	RequestResult getRoomState();
};