#pragma once
#include "TriviaException.h"

class DatabaseException : public TriviaException
{
public:
	DatabaseException(const std::string& data) : TriviaException(data) {}
};