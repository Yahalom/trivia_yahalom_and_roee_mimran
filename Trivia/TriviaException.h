#pragma once
#include <exception>
#include <iostream>

class TriviaException : public std::exception
{
public:
	TriviaException(const std::string& data) : _data(data) {}

	void what() { std::cerr << _data << std::endl; }

private:
	std::string _data;
};