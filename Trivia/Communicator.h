#pragma once
#pragma comment(lib, "Ws2_32.lib")
#include <vector>
#include <bitset>
#include <sstream>
#include "Constants.h"
#include "ClientMap.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "CommunicationHelper.h"

// declartion of LoginRequestHandler prevent circular dependency
class LoginRequestHandler;
class RequestHandlerFactory;
class CommunicationHelper;

enum MessageCodes
{
	LoginResponseCode = 0,
	SignupResponseCode,
	ErrorResponseCode,
	LoginRequestCode,
	SignupRequestCode,
	LogoutRequestCode,
	LogoutResponseCode,
	GetRoomsResponseCode,
	GetPlayersInRoomResponseCode,
	GetStatisticsResponseCode,
	JoinRoomResponseCode,
	CreateRoomResponseCode,
	GetRoomsRequestCode,
	GetPlayersInRoomRequestCode,
	GetStatisticsRequestCode,
	JoinRoomRequestCode,
	CreateRoomRequestCode,
	GetTopScoreRequestCode,
	GetTopScoreResponseCode,
	CloseRoomRequestCode,
	CloseRoomResponseCode,
	StartGameRequestCode,
	StartGameResponseCode,
	GetRoomStateRequestCode,
	GetRoomStateResponseCode,
	leaveRoomRequestCode,
	leaveRoomResponseCode,
	GetQuestionRequestCode,
	GetQuestionResponseCode,
	SubmitAnswerRequestCode,
	SubmitAnswerResponseCode,
	GetGameResultsRequestCode,
	GetGameResultsResponseCode
};

class Communicator
{
public:
	Communicator(RequestHandlerFactory* factory);
	~Communicator();

	void startHandleRequests();

private:
	ClientMap _clients; // changed '_clients' from the original design in the UML to also contains the corresponding client thread and have a mutex locking mechanism for thread synchronization. 
	SOCKET _serverSocket;
	WSAInitializer _wsaInit;
	RequestHandlerFactory* _factory;

	void bindAndListen();
	void handleNewClient(SOCKET clientSocket);

	static RequestInfo receiveMessage(SOCKET source);
};