#include "SqliteDatabase.h"
#include <io.h>

/*
A constructor for class SqliteDatabase.
Input:
	none.

Output:
	none.
*/
SqliteDatabase::SqliteDatabase() : _tableULock(_tableMutex)
{
	int dbExist = _access(SQLITE_DATABASE_PATH, 0); // checking if database exists.

	if (sqlite3_open(SQLITE_DATABASE_PATH, &this->_database) != SQLITE_OK) // opening database (if database does not exists it will be created).
	{
		throw DatabaseException("Error: can not open data base");
	}

	if (dbExist) // if a new data base was created adding its tables.
	{
		this->executeInputQuery(Helper::format("CREATE TABLE %s (%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL UNIQUE, %s TEXT NOT NULL, %s TEXT NOT NULL);", TABLE_USERS, COLUMN_USER_ID, COLUMN_USERNAME, COLUMN_PASSWORD, COLUMN_EMAIL));
		this->executeInputQuery(Helper::format("CREATE TABLE %s (%s UNIQUE NOT NULL, %s FLOAT, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, FOREIGN KEY (%s) REFERENCES %s(%s));", TABLE_STATISTICS, COLUMN_USER_ID, COLUMN_TOTAL_ANSWER_TIME, COLUMN_HIGH_SCORE, COLUMN_NUM_OF_ANSWERS, COLUMN_NUM_OF_CORRECT_ANSWERS, COLUMN_NUM_OF_GAMES, COLUMN_NUM_OF_GAMES_WON, COLUMN_USER_ID, TABLE_USERS, COLUMN_USER_ID));
		this->executeInputQuery(Helper::format("CREATE TABLE %s (%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL);", TABLE_QUESTIONS, COLUMN_QUESTION_ID, COLUMN_QUESTION, COLUMN_CORRECT_ANSWER, COLUMN_WRONG_ANSWERS_LIST));
	}

	this->_tableULock.unlock();
}

/*
A destructor for class SqliteDatabase.
Input:
	none.

Output:
	none.
*/
SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(this->_database);
}

/*
A method that checks if a user exists in the database.
Input:
	username - The name of the user to check.

Output:
	True if the user exists or false if it does not.
*/
bool SqliteDatabase::doesUserExist(const std::string& username)
{
	try
	{
		this->getUserId(username);
	}
	catch (DatabaseException)
	{
		return false;
	}

	return true;
}

/*
A method that checks if a password matches a specific user from the database.
Input:
	username - The name of the user to check.
	password - The password to check.

Output:
	True if the password matches the user or false if it does not.
*/
bool SqliteDatabase::doesPasswordMatch(const std::string& username, const std::string& password)
{
	this->lockTable();

	// executing a select query that gets the user id of the user with the given username and password.
	this->executeOutputQuery(Helper::format("SELECT %s FROM %s WHERE %s = '%s' AND %s = '%s'", COLUMN_USER_ID, TABLE_USERS, COLUMN_USERNAME, username.c_str(), COLUMN_PASSWORD, password.c_str()), this->_table);
	bool match = !this->_table.empty();

	this->unlockTable();
	return match;
}

/*
A method that adds a new user to the database.
Input:
	user - A user to add.

Output:
	True if the user was added or false if it was not.
*/
bool SqliteDatabase::addNewUser(const User& user)
{
	if (this->doesUserExist(user.getUsername()))
	{
		return false;
	}

	this->transaction();

	// executing an INSERT query that adds the given user.
	this->executeInputQuery(Helper::format("INSERT INTO %s (%s, %s, %s) VALUES ('%s', '%s', '%s')", TABLE_USERS, COLUMN_USERNAME, COLUMN_PASSWORD, COLUMN_EMAIL, user.getUsername().c_str(), user.getPassword().c_str(), user.getEmail().c_str()));

	// executing an INSERT query that adds a statistics table for the user.
	this->executeInputQuery(Helper::format("INSERT INTO %s VALUES(%d, 0, 0, 0, 0, 0, 0);", TABLE_STATISTICS, this->getUserId(user.getUsername())));

	if (!this->doesUserExist(user.getUsername()))
	{
		this->rollback();
		return false;
	}

	this->commit();
	return true;
}

/*
A method that returns all the statistics of a user.
Input:
	username - The name of the user to get the statistics from.

Output:
	All the statistics of a user.
*/
userStatistics SqliteDatabase::getUserStatistics(const std::string& username)
{
	this->lockTable();

	// executing a SELECT query that gets all the statistics of the user with the given username.
	this->executeOutputQuery(Helper::format("SELECT %s.* FROM %s INNER JOIN %s on %s.%s = %s.%s WHERE %s.%s == '%s'", TABLE_STATISTICS, TABLE_STATISTICS, TABLE_USERS, TABLE_USERS, COLUMN_USER_ID, TABLE_STATISTICS, COLUMN_USER_ID, TABLE_USERS, COLUMN_USERNAME, username.c_str()), this->_table);

	if (this->_table.empty())
	{
		this->_tableULock.unlock();
		throw DatabaseException("Error: user with username '" + username + "' was not found");
	}

	userStatistics userStats;

	userStats.username = username;
	userStats.totalAnswerTime = std::stof(this->_table[0].value(COLUMN_TOTAL_ANSWER_TIME, ERR_COLUMN_NOT_FOUND));
	userStats.highScore = std::stoi(this->_table[0].value(COLUMN_HIGH_SCORE, ERR_COLUMN_NOT_FOUND));
	userStats.numOfAnswers = std::stoi(this->_table[0].value(COLUMN_NUM_OF_ANSWERS, ERR_COLUMN_NOT_FOUND));
	userStats.numOfCorrectAnswers = std::stoi(this->_table[0].value(COLUMN_NUM_OF_CORRECT_ANSWERS, ERR_COLUMN_NOT_FOUND));
	userStats.numOfGames = std::stoi(this->_table[0].value(COLUMN_NUM_OF_GAMES, ERR_COLUMN_NOT_FOUND));
	userStats.numOfGamesWon = std::stoi(this->_table[0].value(COLUMN_NUM_OF_GAMES_WON, ERR_COLUMN_NOT_FOUND));

	this->unlockTable();
	return userStats;
}

/*
A method that returns all the statistics of the top users.
Input:
	numOfusers - The number of top users to return.

Output:
	All the statistics of the top users.
*/
user_statistics_list SqliteDatabase::getTopUsers(unsigned int numOfusers)
{
	this->lockTable();
	table topUsers = this->_table;
	this->unlockTable();

	// executing a SELECT query that gets all the statistics of the top given number of users.
	this->executeOutputQuery(Helper::format("SELECT %s.*, %s.%s FROM %s INNER JOIN %s on %s.%s = %s.%s ORDER BY %s.%s DESC LIMIT %d;", TABLE_STATISTICS, TABLE_USERS, COLUMN_USERNAME, TABLE_STATISTICS, TABLE_USERS, TABLE_USERS, COLUMN_USER_ID, TABLE_STATISTICS, COLUMN_USER_ID, TABLE_STATISTICS, COLUMN_NUM_OF_GAMES_WON, numOfusers), topUsers);

	if (topUsers.empty())
	{
		throw DatabaseException("Error: no users found");
	}

	user_statistics_list userStatList;

	for (auto it = topUsers.begin(); it != topUsers.end(); ++it)
	{

		userStatList.push_back(userStatistics{ it->value(COLUMN_USERNAME, ERR_COLUMN_NOT_FOUND),
			std::stof(it->value(COLUMN_TOTAL_ANSWER_TIME, ERR_COLUMN_NOT_FOUND)),
			std::stoi(it->value(COLUMN_HIGH_SCORE, ERR_COLUMN_NOT_FOUND)),
			std::stoi(it->value(COLUMN_NUM_OF_ANSWERS, ERR_COLUMN_NOT_FOUND)),
			std::stoi(it->value(COLUMN_NUM_OF_CORRECT_ANSWERS, ERR_COLUMN_NOT_FOUND)),
			std::stoi(it->value(COLUMN_NUM_OF_GAMES, ERR_COLUMN_NOT_FOUND)),
			std::stoi(it->value(COLUMN_NUM_OF_GAMES_WON, ERR_COLUMN_NOT_FOUND)) });
	}

	return userStatList;
}

/*
A method that returns a specific number of questions.
Input:
	numOfQuestions - The number of questions to return.

Output:
	A specific number of questions.
*/
questions SqliteDatabase::getQuestions(unsigned int numOfQuestions)
{
	this->lockTable();
	table questionsTable = this->_table;
	this->unlockTable();

	srand((unsigned)time(NULL));
	this->executeOutputQuery(Helper::format("SELECT * FROM %s ORDER BY RANDOM() LIMIT %d", TABLE_QUESTIONS, numOfQuestions), questionsTable);

	if (questionsTable.size() < numOfQuestions)
	{
		throw DatabaseException("Error: not enough question in the data base");
	}

	questions dbQuestions;

	for (auto it = questionsTable.begin(); it != questionsTable.end(); ++it)
	{
		answers currAnswers;
		std::vector<std::string> currWrongAnswers = Helper::stringToVector(it->value(COLUMN_WRONG_ANSWERS_LIST, ERR_COLUMN_NOT_FOUND));
		
		int insertIndex = rand() % 4;
	
		for (int i = 0, counter = 0; i < 4; i++)
		{
			if (i == insertIndex)
			{
				currAnswers.push_back(it->value(COLUMN_CORRECT_ANSWER, ERR_COLUMN_NOT_FOUND));
			}
			else
			{
				currAnswers.push_back(*(currWrongAnswers.begin() + counter));
				counter++;
			}
		}

		dbQuestions.push_back(Question(it->value(COLUMN_QUESTION, ERR_COLUMN_NOT_FOUND),
			it->value(COLUMN_CORRECT_ANSWER, ERR_COLUMN_NOT_FOUND),
			currAnswers));
	}

	return dbQuestions;
}

/*
A method that adds the game statistics of a user into the overall statistics.
Input:
	gameStats - The game statistics of a user.

Output:
	none.
*/
void SqliteDatabase::updateUserStatistics(const inGameUserStatistics& gameStats)
{
	userStatistics userStats = this->getUserStatistics(gameStats.username);

	if (gameStats.inGameScore > userStats.highScore)
	{
		userStats.highScore = gameStats.inGameScore;
	}

	if (gameStats.won)
	{
		userStats.numOfGamesWon++;
	}

	// executing an UPDATE query that updates the users statistics.
	this->executeInputQuery(Helper::format("UPDATE %s SET %s = %f, %s = %d, %s = %d, %s = %d, %s = %d, %s = %d WHERE %s == %d;", TABLE_STATISTICS, COLUMN_TOTAL_ANSWER_TIME, userStats.totalAnswerTime + gameStats.totalInGameAnswerTime, COLUMN_HIGH_SCORE, userStats.highScore, COLUMN_NUM_OF_ANSWERS, userStats.numOfAnswers + gameStats.inGameCorrectAnswerCount + gameStats.inGameWrongAnswerCount, COLUMN_NUM_OF_CORRECT_ANSWERS, userStats.numOfCorrectAnswers + gameStats.inGameCorrectAnswerCount, COLUMN_NUM_OF_GAMES, userStats.numOfGames + 1, COLUMN_NUM_OF_GAMES_WON, userStats.numOfGamesWon, COLUMN_USER_ID, this->getUserId(gameStats.username)));
}

/*
A method that returns the user id of a user.
Input:
	username - The name of the user to find.

Output:
	The user id of a user.
*/
int SqliteDatabase::getUserId(const std::string& username)
{
	this->lockTable();

	// executing a SELECT query that gets the user id of the user with the given username.
	this->executeOutputQuery(Helper::format("SELECT %s FROM %s WHERE %s == '%s';", COLUMN_USER_ID, TABLE_USERS, COLUMN_USERNAME, username.c_str()), this->_table);

	if (this->_table.empty())
	{
		this->_tableULock.unlock();
		throw DatabaseException("Error: user with username '" + username + "' was not found");
	}

	int id = std::stoi((std::string)this->_table[0].value(COLUMN_USER_ID, ERR_COLUMN_NOT_FOUND));
	this->unlockTable();

	return id;
}

template <class T>
int callback(T data, int argc, char** argv, char** azColName)
{
	row newRow;

	for (int i = 0; i < argc; i++)
	{
		newRow[azColName[i]] = argv[i];
	}

	((table*)data)->push_back(newRow);

	return 0;
}

/*
A method that builds and executes a query that returns an output (for example "SELECT").
Input:
	query - A query to execute.
	outputTable - A table to contain the output of the query.

Output:
	none.
*/
void SqliteDatabase::executeOutputQuery(const std::string& query, table& outputTable)
{
	outputTable.clear();
	sqlite3_exec(this->_database, query.c_str(), callback, &outputTable, nullptr);
}

/*
A method that builds and executes a query that changes the database (for example "INSERT", "DELETE", etc...).
Input:
	query - A query to execute.

Output:
	none.
*/
void SqliteDatabase::executeInputQuery(const std::string& query)
{
	sqlite3_exec(this->_database, query.c_str(), nullptr, nullptr, nullptr);
}

/*
A method that starts a transaction.
Input:
	none.

Output:
	none.
*/
void SqliteDatabase::transaction()
{
	this->executeInputQuery("BEGIN;");
}

/*
A method that rolls back a transaction.
Input:
	none.

Output:
	none.
*/
void SqliteDatabase::rollback()
{
	this->executeInputQuery("ROLLBACK;");
}

/*
A method that commits a transaction.
Input:
	none.

Output:
	none.
*/
void SqliteDatabase::commit()
{
	this->executeInputQuery("COMMIT;");
}

void SqliteDatabase::lockTable()
{
	bool locked = false;

	while (!locked)
	{
		try
		{
			this->_tableULock.lock(); // For some reason occasionally when locking the system_error "resource deadlock would occur" is being thrown but by using try and catch and trying to lock again I can lock with out the exception.
			locked = true;
		}
		catch (std::system_error err) 
		{
			std::cout << err.what();
		}
	}
	
}

void SqliteDatabase::unlockTable()
{
	this->_tableULock.unlock();
}
