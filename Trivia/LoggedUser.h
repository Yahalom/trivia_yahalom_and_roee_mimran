#pragma once
#include <string>
#include "WSAInitializer.h"
#include <iostream>

class LoggedUser
{
public:
	LoggedUser(std::string username, SOCKET socket);

	std::string getUsername() const;
	SOCKET getSocket() const;

	bool operator==(const LoggedUser& other) const;
	bool operator>(const LoggedUser& other) const;
	bool operator<(const LoggedUser& other) const;

private:
	SOCKET _socket;
	std::string _username;
};