#include "Room.h"
#include <iostream>

/*
A constructor for class Room.
Input:
	data - The room data.

Output:
	none.
*/
Room::Room(const RoomData& data)
{
	this->_metadata = data;
}

/*
A method that adds a user to the room.
Input:
	user - A user to add to the room.

Output:
	none.
*/
void Room::addUser(const LoggedUser& user)
{
	if (this->_inRoomUsers.size() == this->_metadata.maxPlayers)
	{
		throw RoomException("Error: the room is full");
	}

	for (auto it = this->_inRoomUsers.begin(); it != this->_inRoomUsers.end(); ++it)
	{
		if (it->getUsername() == user.getUsername())
		{
			throw RoomException("Error: user '" + user.getUsername() + "' is already in the room");
		}
	}

	this->_inRoomUsers.push_back(user);
	this->updateStatus();
}

/*
A method that removes a user from the room.
Input:
	user - A user to remove from the room.

Output:
	none.
*/
void Room::removeUser(const LoggedUser& user)
{
	for (auto it = this->_inRoomUsers.begin(); it != this->_inRoomUsers.end(); ++it)
	{
		if (it->getUsername() == user.getUsername())
		{
			this->_inRoomUsers.erase(it);
			this->updateStatus();
			return;
		}
	}

	throw RoomException("Error: user '" + user.getUsername() + "' was not found");
}

/*
A method returns a all the users that are in the room.
Input:
	none.

Output:
	All the users that are in the room.
*/
const std::vector<LoggedUser>& Room::getAllUsers() const
{
	return this->_inRoomUsers;
}

/*
A method returns the data of the room.
Input:
	none.

Output:
	The data of the room.
*/
const RoomData& Room::getData() const
{
	return this->_metadata;
}

/*
A method that removes all the users from the room and closes it.
Input:
	none.

Output:
	none.
*/
void Room::close()
{
	this->_inRoomUsers.clear();
	this->updateStatus();
}

/*
A method checks and updates the status of the room.
Input:
	none.

Output:
	none.
*/
void Room::updateStatus()
{
	if (this->_inRoomUsers.size() == 0)
	{
		this->_metadata.isActive = false;
	}
	else
	{
		this->_metadata.isActive = true;
	}
}