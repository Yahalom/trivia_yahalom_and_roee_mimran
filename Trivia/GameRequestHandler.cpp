#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(LoggedUser user, GameManager* gameManager, RequestHandlerFactory* factory, Game* game, int roomId) :
	_game(game), _user(user), _gameManager(gameManager), _requestHandlerFactory(factory)
{
	this->_gameId = roomId;
}

/*
function check if the request is relevant to the game request handler
input:
	the request info contain the request id

output:
	relevant or not
*/
bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == MessageCodes::leaveRoomRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetQuestionRequestCode) ||
		(requestInfo.RequestId == MessageCodes::SubmitAnswerRequestCode) ||
		(requestInfo.RequestId == MessageCodes::GetGameResultsRequestCode);
}

/*
function handle the request, send the request info to the right function
input:
	the request info contain the buffer and the id

output:
	the request result contain the buffer to send
*/
RequestResult GameRequestHandler::handleRequest(RequestInfo requestInfo, SOCKET clientSocket)
{
	RequestResult result;

	try
	{
		switch (requestInfo.RequestId)
		{
		case MessageCodes::leaveRoomRequestCode:
			result = this->leaveGame(requestInfo);
			break;

		case MessageCodes::GetQuestionRequestCode:
			result = this->getQuestion(requestInfo, clientSocket);
			break;

		case MessageCodes::SubmitAnswerRequestCode:
			result = this->submitAnswer(requestInfo);
			break;

		case MessageCodes::GetGameResultsRequestCode:
			result = this->getGameResults(requestInfo, clientSocket);
			break;

		case MessageCodes::GetRoomStateRequestCode:
			result = this->getRoomState();
			break;
		}
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
		ErrorResponse resp;
		resp.message = "ERROR";
		result.buffer = JsonResponsePacketSerializer::serializeResponse(resp);
		result.newHandler = nullptr;
	}

	return result;
}

/*
function get the question from the game for the user
input:
	the requst of the user contain the buffer

output :
	the request result contain the buffer to send to the user
*/
RequestResult GameRequestHandler::getQuestion(RequestInfo requestInfo, SOCKET clientSocket)
{
	RequestResult result;
	GetQuestionResponse responseQuestion;
	GetAnswersResponse responseAnswers;
	
	Question question = this->_game->getQuestionForUser(this->_user);

	responseQuestion.question = question.getQuestion();

	// send the question now and the answers will be sent in the communicator
	result.buffer = JsonResponsePacketSerializer::serializeResponse(responseQuestion);

	// convert the vector to a unordered map
	const answers answersFromQuestion = question.getAnswers();
	for (int i = 0; i < answersFromQuestion.size(); i++)
	{
		responseAnswers.answers.insert(std::pair<unsigned int, std::string>(i, answersFromQuestion[i]));
	}

	// add the two buffers with 20 as seperator
	result.buffer.push_back(20);
	for (auto byte : JsonResponsePacketSerializer::serializeResponse(responseAnswers))
	{
		result.buffer.push_back(byte);
	}

	result.newHandler = this->_requestHandlerFactory->createGameRequestHandler(this->_user, this->_game, this->_gameId);
	return result;
}

/*
function submit the answer the user afk for
input:
	the requst of the user contain the buffer

output :
	the request result contain the buffer to send to the user
*/
RequestResult GameRequestHandler::submitAnswer(RequestInfo requestInfo)
{
	RequestResult result;
	SubmitAnswerResponse response;
	
	SubmitAnswerRequest request = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(requestInfo.buffer);
	
	response.correctAnswerId = this->_game->submitAnswer(this->_user, request.chosenAnswer);
	response.status = 1;
	
	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_requestHandlerFactory->createGameRequestHandler(this->_user, this->_game, this->_gameId);
	return result;
}

/*
function get the game results from the game
input:
	the requst of the user contain the buffer

output :
	the request result contain the buffer to send to the user
*/
RequestResult GameRequestHandler::getGameResults(RequestInfo requestInfo, SOCKET clientSocket)
{
	GetGameResultsResponse response;
	RequestResult result;
	PlayerResult curr;
	buffer currBuffer;
	std::vector<inGameUserStatistics> stats;
	inGameUserStatistics currStats;

	this->_game->waitForEndGame();

	for (inGamePlayer player : this->_game->getInGamePlayers())
	{
		curr.username = player.first.getUsername();
		curr.correctAnswersCount = player.second.correctAnswerCount;
		curr.wrongAnswerCount = player.second.wrongAnswerCount;
		curr.averageAnswerTime = Helper::getAverageTime(player.second);
		curr.score = Helper::getHighScore(curr);

		currStats.inGameCorrectAnswerCount = curr.correctAnswersCount;
		currStats.inGameScore = curr.score;
		currStats.inGameWrongAnswerCount = curr.wrongAnswerCount;
		currStats.totalInGameAnswerTime = player.second.totalAnswerTime;
		currStats.username = player.first.getUsername();
		currStats.won = false;
		stats.push_back(currStats);

		response.playerResult = curr;
		response.status = 1;

		currBuffer.push_back('{');
		currBuffer = JsonResponsePacketSerializer::serializeResponse(response);
		currBuffer.push_back('}');
		currBuffer.push_back(',');
		currBuffer.push_back(',');
		currBuffer.push_back(',');

		result.buffer.insert(result.buffer.end(), currBuffer.begin(), currBuffer.end());
		currBuffer.clear();
	}

	result.buffer.pop_back(); // remove the last ,
	result.buffer.pop_back(); // remove the last ,
	result.buffer.pop_back(); // remove the last ,
	
	// get the high score
	inGameUserStatistics* highestScorePlayer = &stats[0];
	
	for (auto stat = stats.begin() + 1; stat != stats.end(); ++stat)
	{
		if (stat->inGameScore > highestScorePlayer->inGameScore)
		{
			// iterator to pointer conversation
			highestScorePlayer = &(*stat);
		}
		else if (stat->inGameScore == highestScorePlayer->inGameScore)
		{
			if (Helper::getAverageTime(*stat) > Helper::getAverageTime(*highestScorePlayer))
			{
				// iterator to pointer conversation
				highestScorePlayer = &(*stat);
			}
			else if (Helper::getAverageTime(*stat) == Helper::getAverageTime(*highestScorePlayer))
			{
				if (stat->inGameCorrectAnswerCount > highestScorePlayer->inGameCorrectAnswerCount)
				{
					// iterator to pointer conversation
					highestScorePlayer = &(*stat);
				}
			}
		}
	}
	highestScorePlayer->won = true;

	// the first thread that gets to this point will update the statistics of all the players in the game (only one thread can enter the "if").
	if (this->_game->updateStatistics())
	{
		for (auto stat : stats)
		{
			this->_requestHandlerFactory->getStatisticsManager().updateUserStatistics(stat);
		}
	}

	result.newHandler = this->leaveGame(requestInfo).newHandler;

	return result;
}

/*
function remove the user from the game
input:
	the requst of the user contain the buffer

output:
	the request result contain the buffer to send to the user
*/
RequestResult GameRequestHandler::leaveGame(RequestInfo requestInfo)
{
	RequestResult result;
	LeaveGameResponse response;

	this->_game->leaveGame(this->_user);

	if (!this->_game->countOnlinePlayers())
	{
		this->_gameManager->deleteGame(this->_game->getName());
	}

	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_requestHandlerFactory->CreateMenuRequestHandler(this->_user.getUsername(), this->_user.getSocket());
	return result;
}

/*
A method that returns the room's stats (the original room is closed).
Input:
	none.

Output:
	The request result contain the buffer to send to the user.
*/
RequestResult GameRequestHandler::getRoomState()
{
	GetRoomStateResponse response;
	RequestResult result;
	Room originalRoom = this->_requestHandlerFactory->getRoomManager().getRoom(this->_gameId);

	RoomData roomData = originalRoom.getData();

	response.hasGameBegun = !roomData.isActive;
	response.questionCount = roomData.questionCount;
	response.timePerQuestion = roomData.timePerQuestion;
	response.status = 1;

	result.buffer = JsonResponsePacketSerializer::serializeResponse(response);
	result.newHandler = this->_requestHandlerFactory->createGameRequestHandler(this->_user, this->_game, this->_gameId);
	return result;
}