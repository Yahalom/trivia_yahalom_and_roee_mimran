#include "ClientMap.h"

/*
A constructor for class ClientMap.
Input:
	none.

Output:
	none.
*/
ClientMap::ClientMap() : _mapULock(_mapMutex)
{
	this->_mapULock.unlock();
}

/*
A destructor for class ClientMap.
Input:
	none.

Output:
	none.
*/
ClientMap::~ClientMap()
{
	for (auto it = this->_clientMap.begin(); it != this->_clientMap.end(); ++it)
	{
		this->clearEntry(*it);
	}
}

/*
A method that removes a client from the client map.
input:
	clientSocket - The socket of the client to remove.

Output:
	none.
*/
void ClientMap::remove(SOCKET clientSocket)
{
	this->lockMap();

	auto itRemove = this->_clientMap.find(clientSocket); // getting the iterator of the client.

	if (itRemove != this->_clientMap.end()) // checking if the client exists in the client map.
	{
		this->clearEntry(*itRemove);
		this->_clientMap.erase(itRemove);
	}

	this->unlockMap();
}

/*
A method that adds a new client to the client map and detaches his corresponding thread.
input:
	clientSocket - The socket of the new client.
	requestHandler - The request handler of the new client.
	clientThread - The thread of the new client.

Output:
	none.
*/
void ClientMap::insert(SOCKET clientSocket, IRequestHandler* requestHandler, std::thread* clientThread)
{
	if (!clientThread)
	{
		throw ThreadException("Error: user thread can not be detached");
	}

	this->lockMap();

	this->_clientMap.insert(client_Map_entry(clientSocket, client_data(requestHandler, clientThread)));
	clientThread->detach();

	this->unlockMap();
}

/*
A method that changes the request handler of the client map.
input:
	clientSocket - The socket of the client that his request handler should be changed.
	requestHandler - The new request handler.

Output:
	none.
*/
void ClientMap::setRequestHandler(SOCKET clientSocket, IRequestHandler* requestHandler)
{
	this->lockMap();

	if (this->_clientMap.find(clientSocket) != this->_clientMap.end()) // checking if the client exists in the client map.
	{
		delete this->_clientMap[clientSocket].first;
		this->_clientMap[clientSocket].first = requestHandler;
	}

	this->unlockMap();
}

/*
A method that gets the size of the client map.
input:
	none.

Output:
	The size of the client map.
*/
int ClientMap::getSize()
{
	int size;

	this->lockMap();
	size = this->_clientMap.size();
	this->unlockMap();

	return size;
}

/*
A method that gets the client's data by his socket.
input:
	clientSocket - The socket of the wanted client.

Output:
	The copy of the client's data (values will be null if the client was not found).
*/
const client_data ClientMap::operator[](SOCKET clientSocket)
{
	client_data cData(NULL, NULL);

	this->lockMap();

	if (this->_clientMap.find(clientSocket) != this->_clientMap.end()) // checking if the client exists in the client map.
	{
		cData = this->_clientMap[clientSocket];
	}

	this->unlockMap();

	return cData;
}

/*
A method that clears a client map entry.
input:
	entry - The entry to clear.

Output:
	none.
*/
void ClientMap::clearEntry(client_Map_entry entry)
{
	closesocket(entry.first); // closing the client socket (if open)
	delete entry.second.first; // deleting the request handler.
	delete entry.second.second; // deleting the client thread.
}

/*
A method that locks access to the client map.
input:
	none.

Output:
	none.
*/
void ClientMap::lockMap()
{
	this->_mapULock.lock();
}

/*
A method that unlocks access to the client map.
input:
	none.

Output:
	none.
*/
void ClientMap::unlockMap()
{
	this->_mapULock.unlock();
}