#include "LoginManager.h"

/*
A constructor for class LoginManager.
Input:
	database - The database type to use.

Output:
	none.
*/
LoginManager::LoginManager(IDatabase* database)
{
	this->_database = database;
}

/*
A method that adds a new user to the database.
Input:
	user - A user to add.

Output:
	none.
*/
void LoginManager::signup(const User& user, SOCKET clientSocket)
{
	if (!this->_database->addNewUser(user))
	{
		throw LoginManagerException("Error: can not add new user");
	}

	this->_loggedUsers.push_back(LoggedUser(user.getUsername(), clientSocket));
}

/*
A method that adds a user (if it exists) to the logged users list.
Input:
	username - The username of the user to add.
	password - The password of the user to add.

Output:
	none.
*/
void LoginManager::login(const std::string& username, const std::string& password, SOCKET clientSocket)
{
	if (this->findLoggedUser(username) != this->_loggedUsers.end())
	{
		throw LoginManagerException("Error: user already logged in");
	}

	if (!this->_database->doesPasswordMatch(username, password))
	{
		throw LoginManagerException("Error: username and password does not match");
	}

	this->_loggedUsers.push_back(LoggedUser(username, clientSocket));
}

/*
A method that removes a user from the logged users list.
Input:
	username - The username of the user to remove.

Output:
	none.
*/
void LoginManager::logout(const std::string& username)
{
	auto userIt = this->findLoggedUser(username);

	if (userIt == this->_loggedUsers.end())
	{
		throw LoginManagerException("Error: user is not logged in");
	}

	this->_loggedUsers.erase(userIt);
}

std::vector<LoggedUser>::iterator LoginManager::findLoggedUser(const std::string& username)
{
	auto it = this->_loggedUsers.begin();

	for (it; it != this->_loggedUsers.end(); ++it)
	{
		if (it->getUsername() == username)
		{
			break;
		}
	}

	return it;
}