#pragma once

#include "IRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoginManagerException.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include <iostream>
#include "User.h"


// declartion of communication prevent circular dependency
class Communicator;
class RequestHandlerFactory;
class MenuRequestHandler;

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* _handleFactory;

	RequestResult login(LoginRequest request, SOCKET clientSocket);
	RequestResult signup(SignupRequest request, SOCKET clientSocket);

public:
	LoginRequestHandler(RequestHandlerFactory* handleFactory);
	~LoginRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo, SOCKET clientSocket);
};

