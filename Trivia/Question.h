#pragma once
#include <string>
#include <vector>

typedef std::vector<std::string> answers;

class Question
{
public:
	Question(const std::string& question, const std::string& correctAnswer, const answers& allAnswers);

	const std::string getQuestion() const;
	const answers getAnswers() const;
	const std::string getCorrectAnswer();

private:
	std::string _question;
	std::string _correctAnswer;
	answers _answers; // all the answers (including the correct one).
};

typedef std::vector<Question> questions;