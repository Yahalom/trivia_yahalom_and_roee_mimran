#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory()
{
	this->_dataBase = new SqliteDatabase;
	this->_loginManager = new LoginManager(this->_dataBase);
	this->_statisticsManage = new StatisticsManager(this->_dataBase);
	this->_gameManager = new GameManager(this->_dataBase);
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	// delete the login manager
	// loginManager's d'tor will delete dataBase
	delete this->_loginManager;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return *this->_loginManager;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this);
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return *this->_statisticsManage;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->_roomManager;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return *this->_gameManager;
}

MenuRequestHandler* RequestHandlerFactory::CreateMenuRequestHandler(std::string userName, SOCKET clientSocket)
{
	return new MenuRequestHandler(LoggedUser(userName, clientSocket), &(this->_roomManager), this, this->_statisticsManage);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(std::string userName, SOCKET clientSocket, Room* room)
{
	return new RoomAdminRequestHandler(LoggedUser(userName, clientSocket), &(this->_roomManager), this, room);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(std::string userName, SOCKET clientSocket, Room* room)
{
	return new RoomMemberRequestHandler(LoggedUser(userName, clientSocket), &(this->_roomManager), this, room);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Game* game, int roomId)
{
	return new GameRequestHandler(user, this->_gameManager, this, game, roomId);
}
