#pragma once
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoginManager.h"
#include "SqliteDatabase.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#include "Game.h"
#include "GameManager.h"

// early decleration  to prevent circular dependancy
class LoginRequestHandler;
class MenuRequestHandler;
class StatisticsManager;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;
class GameManager;
class Game;

class RequestHandlerFactory
{
private:
	LoginManager* _loginManager;
	IDatabase* _dataBase;
	StatisticsManager* _statisticsManage;
	GameManager* _gameManager;
	RoomManager _roomManager;

public:
	RequestHandlerFactory();
	~RequestHandlerFactory();

	// getters
	LoginManager& getLoginManager();
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	GameManager& getGameManager();

	// creaters
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* CreateMenuRequestHandler(std::string userName, SOCKET clientSocket);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(std::string userName, SOCKET clientSocket, Room* room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(std::string userName, SOCKET clientSocket, Room* room);
	GameRequestHandler* createGameRequestHandler(LoggedUser user, Game* game, int roomId);
};