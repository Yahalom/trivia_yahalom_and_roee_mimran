#pragma once
#include "TriviaException.h"

class ThreadException : public TriviaException
{
public:
	ThreadException(const std::string& data) : TriviaException(data) {}
};