#pragma once
#include <map>
#include <mutex>
#include <thread>
#include "Constants.h"
#include "WSAInitializer.h"
#include "IRequestHandler.h"

typedef std::pair<IRequestHandler*, std::thread*> client_data;
typedef std::pair<SOCKET, client_data> client_Map_entry;
typedef std::map<SOCKET, client_data> client_map;

class ClientMap
{
public:
	ClientMap();
	~ClientMap();

	void remove(SOCKET clientSocket);
	void insert(SOCKET clientSocket, IRequestHandler* requestHandler, std::thread* clientThread);
	void setRequestHandler(SOCKET clientSocket, IRequestHandler* requestHandler);

	int getSize();
	const client_data operator[](SOCKET clientSocket);

private:
	client_map _clientMap;
	std::mutex _mapMutex;
	std::unique_lock<std::mutex> _mapULock;

	void clearEntry(client_Map_entry entry);

	void lockMap();
	void unlockMap();
};

