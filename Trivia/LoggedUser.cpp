#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username, SOCKET socket)
{
	this->_username = username;
	this->_socket = socket;
}

std::string LoggedUser::getUsername() const
{
	return this->_username;
}


SOCKET LoggedUser::getSocket() const
{
	return this->_socket;
}

bool LoggedUser::operator==(const LoggedUser& other) const
{
	return this->_username == other._username;
}

bool LoggedUser::operator>(const LoggedUser& other) const
{
	return this->_username > other._username;
}

bool LoggedUser::operator<(const LoggedUser& other) const
{
	return this->_username < other._username;
}

