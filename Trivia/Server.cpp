#include "Server.h"
#include <string>

/*
c'tor
A method that initlized the communicator
input: 
	the factory

output:
	none
*/
Server::Server() : _communicator(&this->_factory)
{
}

/*
A method that detaches a communicator thread and executes commands from the console.
Input:
	none.

Output:
	none.
*/
void Server::run()
{
	std::thread tConnector(&Communicator::startHandleRequests, &this->_communicator);
	tConnector.detach();

	std::cout << "server listening... | type \"" << COMMAND_EXIT << "\" to close the server\n\n" << std::endl;
	std::string input = "";

	while (input != COMMAND_EXIT)
	{
		std::getline(std::cin, input);
	}
}