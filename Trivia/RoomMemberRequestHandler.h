#pragma once

#include "LoggedUser.h"
#include "Room.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"
#include "Constants.h"

class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser loggedUser, RoomManager* roomManager, RequestHandlerFactory* handlerFactory, Room* room);

	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo, SOCKET socket);

private:
	LoggedUser _user;
	Room* _room;
	RoomManager* _roomManager;
	RequestHandlerFactory* _handlerFactory;

	RequestResult leaveRoom(RequestInfo);
	RequestResult getRoomState(RequestInfo);
	RequestResult startGame(RequestInfo requestInfo);
};

