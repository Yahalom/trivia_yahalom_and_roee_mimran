#include "Game.h"

/*
A constructor for class Game.
Input:
	gameRoom - A room to start a game in.
	gameQuestions - Questions for the game.

Output:
	none.
*/
Game::Game(const Room& gameRoom, const questions& gameQuestions) :
	_questionULock(_questionMutex), _playersULock(_playersMutex), _endgameCounterULock(_endGameCounterMutex), _updateStatsULock(_updateStatsMutex)
{
	std::vector<LoggedUser> usersInRoom = gameRoom.getAllUsers();
	
	for (auto& player : usersInRoom)
	{
		this->_players.insert(inGamePlayer(player, GameData{ 0, 0, 0, 0, true }));
	}

	this->_questions = gameQuestions;
	this->_name = gameRoom.getData().name;
	this->_endGamePlayersCounter = 0;

	this->_wereStatsUpdated = false;

	this->_playersULock.unlock();
	this->_questionULock.unlock();
	this->_endgameCounterULock.unlock();
	this->_updateStatsULock.unlock();
}

const std::string Game::getName()
{
	return this->_name;
}

/*
A method that returns a vector of all the player with their data.
Input:
	none.

Output:
	A vector of all the player with their data.
*/
const inGamePlayers Game::getInGamePlayers()
{
	inGamePlayers players;

	this->lockPlayers();
	players = this->_players;
	this->unlockPlayers();

	return players;
}

/*
A method that gets the current question for a specific player and starts a timer.
Input:
	player - A player to get a question to.

Output:
	The current question for a specific player.
*/
const Question Game::getQuestionForUser(const LoggedUser& player)
{
	this->lockPlayers();
	auto gamePlayer = this->findPlayer(player);
	this->unlockPlayers();

	gamePlayer->second.startTime = std::chrono::high_resolution_clock::now(); // Saving the start time.

	return this->getCurrentQuestion(*gamePlayer);
}

/*
A method that submits an answer from a specific player and saves their answer time.
Input:
	submitter - The player that submited the answer.
	answerId - The id of the submitted answer.

Output:
	the correct answer id
*/
unsigned int Game::submitAnswer(const LoggedUser& submitter, unsigned int answerId)
{
	this->lockPlayers();
	auto gamePlayer = this->findPlayer(submitter);
	this->unlockPlayers();

	if (answerId > MAX_QUESTIONS || answerId == 0)
	{
		throw GameException("Error: answer id is out of range");
	}

	Question currQuestion = this->getCurrentQuestion(*gamePlayer);

	// Adding the answer time of this question to the total answer time.
	gamePlayer->second.totalAnswerTime += (float)std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - gamePlayer->second.startTime).count() / 1000;

	gamePlayer->second.currentQuestionIndex++;

	if (answerId != TIME_OVER && currQuestion.getCorrectAnswer() == currQuestion.getAnswers()[answerId - 1])
	{
		gamePlayer->second.correctAnswerCount++;
		return answerId;
	}

	gamePlayer->second.wrongAnswerCount++;

	// find the correct answer and return his id
	for (unsigned int i = 1; i < MAX_QUESTIONS; i++)
	{
		if (currQuestion.getCorrectAnswer() == currQuestion.getAnswers()[i - 1])
		{
			return i;
		}
	}
}

/*
A method that removes a player from the game.
Input:
	player - A player to remove.

Output:
	none.
*/
void Game::removePlayer(const LoggedUser& player)
{
	this->lockPlayers();
	this->_players.erase(findPlayer(player));
	this->unlockPlayers();
}

/*
A method that changes the players status from from "in game" to "not in game".
Input:
	player - A player to change.

Output:
	none.
*/
void Game::leaveGame(const LoggedUser& player)
{
	this->lockPlayers();
	this->findPlayer(player)->second.isInGame = false;
	this->unlockPlayers();
}

void Game::lockStatisticsUpdating()
{
	bool locked = false;

	while (!locked)
	{
		try
		{
			this->_updateStatsULock.lock(); // For some reason occasionally when locking the system_error "resource deadlock would occur" is being thrown but by using try and catch and trying to lock again I can lock with out the exception.
			locked = true;
		}
		catch (std::system_error) {}
	}
}

void Game::unlockStatisticsUpdating()
{
	this->_updateStatsULock.unlock();
}

void Game::lockQuestion()
{
	bool locked = false;

	while (!locked)
	{
		try
		{
			this->_questionULock.lock(); // For some reason occasionally when locking the system_error "resource deadlock would occur" is being thrown but by using try and catch and trying to lock again I can lock with out the exception.
			locked = true;
		}
		catch (std::system_error) {}
	}
}

void Game::unlockQuestion()
{
	this->_questionULock.unlock();
}

void Game::lockPlayers()
{
	bool locked = false;

	while (!locked)
	{
		try
		{
			this->_playersULock.lock(); // For some reason occasionally when locking the system_error "resource deadlock would occur" is being thrown but by using try and catch and trying to lock again I can lock with out the exception.
			locked = true;
		}
		catch (std::system_error) {}
	}
}

void Game::unlockPlayers()
{
	this->_playersULock.unlock();
}

void Game::lockEndGameCounter()
{
	bool locked = false;

	while (!locked)
	{
		try
		{
			this->_endgameCounterULock.lock(); // For some reason occasionally when locking the system_error "resource deadlock would occur" is being thrown but by using try and catch and trying to lock again I can lock with out the exception.
			locked = true;
		}
		catch (std::system_error) {}
	}
}

void Game::unlockEndGameCounter()
{
	this->_endgameCounterULock.unlock();
}

void Game::waitForEnd()
{
	std::unique_lock<std::mutex> gameULock(this->_gameMutex);

	this->_gameCondition.wait(gameULock, [this]() {return this->getEndGamePlayersCounter() == this->getPlayersCount(); });
}

void Game::EndGame()
{
	this->_gameCondition.notify_all();
}

/*
A method that count all the players with the status "in game".
Input:
	none.

Output:
	The number of all the players with the status "in game".
*/
int Game::countOnlinePlayers()
{
	int counter = 0;

	this->lockPlayers();
	auto players = this->_players;
	this->unlockPlayers();

	for (auto& player : players)
	{
		if (player.second.isInGame)
		{
			counter++;
		}
	}

	return counter;
}

bool Game::updateStatistics()
{
	bool result = false;

	this->lockStatisticsUpdating();

	if (!this->_wereStatsUpdated)
	{
		this->_wereStatsUpdated = true;
		result = true;
	}

	this->unlockStatisticsUpdating();

	return result;
}

void Game::waitForEndGame()
{
	this->lockEndGameCounter();
	this->_endGamePlayersCounter++;
	this->unlockEndGameCounter();

	if (this->getEndGamePlayersCounter() < this->getPlayersCount())
	{
		this->waitForEnd();
	}
	else
	{
		this->EndGame();
	}
}

const int Game::getEndGamePlayersCounter()
{
	this->lockEndGameCounter();
	int counter = this->_endGamePlayersCounter;
	this->unlockEndGameCounter();

	return counter;
}

const int Game::getPlayersCount()
{
	// function getInGamePlayers already has a lock
	int count = this->getInGamePlayers().size();

	return count;
}

/*
A method that searches for a player.
Input:
	player - A player to search.

Output:
	A player.
*/
inGamePlayers::iterator Game::findPlayer(const LoggedUser& player)
{
	auto playerIt = this->_players.find(player);

	if (playerIt == this->_players.end())
	{
		throw GameException("Error: the player \"" + player.getUsername() + "\" does not exists in the game");
	}

	return playerIt;
}

/*
A method that gets the current question for a specific player.
Input:
	player - A player to get a question to.

Output:
	The current question for a specific player.
*/
Question Game::getCurrentQuestion(const inGamePlayer& player)
{
	this->lockQuestion();
	if (player.second.currentQuestionIndex >= this->_questions.size())
	{
		this->unlockQuestion();
		throw GameException("Error: player \"" + player.first.getUsername() + "\" already completed all the questions");
	}

	auto question = this->_questions[player.second.currentQuestionIndex];
	this->unlockQuestion();

	return question;
}