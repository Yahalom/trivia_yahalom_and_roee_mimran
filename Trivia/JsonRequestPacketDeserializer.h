#pragma once
#include "Constants.h"
#include "StructRequests.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(buffer bufferTodeserialize);
	static SignupRequest deserializeSignupRequest(buffer bufferTodeserialize);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(buffer bufferTodeserialize);
	static JoinRoomRequest deserializeJoinRoomRequest(buffer bufferTodeserialize);
	static CreateRoomRequest deserializeCreateRoomRequest(buffer bufferTodeserialize);

	static SubmitAnswerRequest deserializerSubmitAnswerRequest(buffer bufferTodeserialize);

	static unsigned int deserializeMessageDataLength(buffer buff);
	
private:
	static json extract(buffer buff);
};

