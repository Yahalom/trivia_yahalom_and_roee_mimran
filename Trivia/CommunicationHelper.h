#pragma once

#include "Communicator.h"

class Communicator;

class CommunicationHelper
{
public:
	static void sendString(SOCKET destination, const std::string& data);
	static std::string receiveString(SOCKET source, int maxSize);
	static void sendBuffer(SOCKET destination, const buffer& data);
	static buffer receiveBuffer(SOCKET source, int maxSize);
	static void sendBufferToSomeUsers(std::vector<SOCKET> users, const buffer& data);
};