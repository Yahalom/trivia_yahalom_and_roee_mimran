#pragma once
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(LoggedUser loggedUser, RoomManager* roomManager, RequestHandlerFactory* handlerFactory, StatisticsManager* statisticsManager);

	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo, SOCKET clientSocket);

private:
	LoggedUser _user;
	RoomManager* _roomManager;
	StatisticsManager* _statisticsManager;
	RequestHandlerFactory* _handlerFactory;

	RequestResult signout(RequestInfo request);
	RequestResult getRooms(RequestInfo request);
	RequestResult getPlayersInRoom(RequestInfo request);
	RequestResult getStatistics(RequestInfo request);
	RequestResult joinRoom(RequestInfo request);
	RequestResult createRoom(RequestInfo request);
	RequestResult getTopScore(RequestInfo request);
};

