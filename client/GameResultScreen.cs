﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Text.RegularExpressions;


namespace Game
{
    public partial class GameResultScreen : Form
    {
        private client.Socket _socket = null;
        private string _response;

        public GameResultScreen(client.Socket socket, string response)
        {
            this._socket = socket;
            this._response = response;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<PlayerResults> playerResults = new List<PlayerResults>();
            string[] responseSplited = Regex.Split(this._response, ",,,");
            PlayerResults currNode = new PlayerResults();
            string[] resultSeperated;
            string highestScoreName;

            foreach (var player in responseSplited)
            {
                GetGameResultResponse decodedPlayer = client.BsonConverts.fromBson<GetGameResultResponse>(player);

                // remove the first []
                decodedPlayer.playerResults = decodedPlayer.playerResults.Substring(1);
                decodedPlayer.playerResults = decodedPlayer.playerResults.Remove(decodedPlayer.playerResults.Length - 1);

                resultSeperated = decodedPlayer.playerResults.Split(',');
                currNode.username = resultSeperated[0].Split(':')[1];
                currNode.correctAnswers = Convert.ToUInt32(resultSeperated[1].Split(':')[1]);
                currNode.averageAnswerTime = float.Parse(resultSeperated[3].Split(':')[1]);
                currNode.score = Convert.ToUInt32(resultSeperated[4].Split(':')[1]);

                playerResults.Add(currNode.getCopy());
            }

            // get the player with the max score and show it
            this.winner.Text += playerResults.Aggregate((curMax, x) => (curMax == null || x.score > curMax.score ? x : curMax)).username;

            this.dataGridView1.DataSource = playerResults;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            new Menu.MainMenuScreen(this._socket).ShowDialog();
            System.Environment.Exit(1);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }

    class GetGameResultResponse
    {
        public string playerResults;
    }

    class PlayerResults
    {
        public string username { get; set; }
        public uint correctAnswers { get; set; }
        public float averageAnswerTime { get; set; }
        public uint score { get; set; }

        public PlayerResults getCopy()
        {
            return new PlayerResults { averageAnswerTime = this.averageAnswerTime, correctAnswers = this.correctAnswers, score = this.score, username = this.username };
        }
    }

}
