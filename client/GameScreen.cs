﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Threading;
using Newtonsoft.Json.Linq;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;


namespace Game
{
    public partial class GameScreen : Form
    {
        private client.Socket _socket = null;
        private Menu.GetRoomStateResponse _roomState;
        private int _timeLeft = 0;
        private uint _currentRound = 0;
        private uint _chosenAnswer = 0;
        private bool _isTimeOver = false;
        private bool _hasAnswerd = false;

        private Thread _timeManagerThread;
        private Thread _communicatorThread;

        public GameScreen(client.Socket socket, Menu.GetRoomStateResponse roomState)
        {
            this._socket = socket;
            this._roomState = roomState;
            this._timeLeft = Convert.ToInt32(this._roomState.timePerQuestion);

            InitializeComponent();
            this.FormClosing += closer;

            this.cover.Visible = false;
            this.waitingPic.Visible = false;
            this.waitSen1.Visible = false;
            this.waitSen2.Visible = false;
            this.waitSen3.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this._communicatorThread = new Thread(this.communicator);
            this._timeManagerThread = new Thread(this.timeManager);

            this._communicatorThread.Start();
            this._timeManagerThread.Start();
        }

        private void systemTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                this.timer.Invoke((MethodInvoker)delegate
                {
                    this._timeLeft -= 1;

                    if(this._timeLeft > 0)
                    {
                        this.timer.Text = this._timeLeft.ToString();
                        this.timer.Value = this._timeLeft * Convert.ToInt32(100 / this._roomState.timePerQuestion); // convert the time left to precents
                    }
                    else
                    {
                        this.timer.Text = "0";
                        this.timer.Value = 0;
                    }
                    
                });
            }
            catch 
            {
                //this._timeManagerThread.Abort();
            }
        }

        private void timeManager()
        {
            // create a timer
            System.Timers.Timer systemTimer = new System.Timers.Timer();
            systemTimer.Interval = 1000;

            // set the systemTimer_Elapsed function
            systemTimer.Elapsed += systemTimer_Elapsed;
            systemTimer.Start();

            // check everytime when the timer is less than 0 seconds
            while(true)
            {
                if (this._timeLeft <= 0)
                {
                    this._isTimeOver = true;
                }
                else
                {
                    this._isTimeOver = false;
                }
            }
        }

        /*
            function communicate with the server and send the data to thje right function
            input: none
            output: none
        */
        private void communicator()
        {
            const uint TIME_OVER = 5;

            while (this._currentRound < this._roomState.questionCount)
            {
                Question askedQuestion = this.getQuestion();
                this.updateScreen(askedQuestion);

                this._timeLeft = Convert.ToInt32(this._roomState.timePerQuestion) + 1;
                this._chosenAnswer = TIME_OVER;
                this._isTimeOver = false;
                this._hasAnswerd = false;

                this.waitForAnswer();
                this.submitAnswer(this._chosenAnswer);

                // wait 3 seconds so he will look at the correct answer
                Thread.Sleep(3000);
                
                this._currentRound++;
            }

            this._timeManagerThread.Abort();

            this.Invoke((MethodInvoker)delegate
            {
                this.waitSen1.Visible = true;
                this.waitSen2.Visible = true;
                this.waitSen3.Visible = true;
            });

            this.Invoke((MethodInvoker)delegate
            {
                this.cover.Visible = true;
                this.waitingPic.Visible = true;
            });


            const int GET_GAME_RESULT_CODE = 31;
            string len = new string((char)0, 4);
            this._socket.sendMsg(((char)GET_GAME_RESULT_CODE).ToString() + len);
            string response = this._socket.getMsg();

            // show the results at the end
            this.Invoke((MethodInvoker)delegate
            {
                this.Visible = false;
                new Game.GameResultScreen(this._socket, response).ShowDialog();
                //this.Close();
            });
            
        }

        private void waitForAnswer()
        {
            while (true)
            {
                if(this._hasAnswerd || this._isTimeOver)
                {
                    break;
                }
            }
        }

        /*
            function get question from the server 
            input:
                none

            output:
                the question form the server
        */
        private Question getQuestion()
        {
            const int GET_QUESTION_CODE = 27;
            string len = new string((char)0, 4);

            this._socket.sendMsg((char)GET_QUESTION_CODE + len);

            string responseString = this._socket.getMsg();
            
            GetQuestionResponse responseQuestion = client.BsonConverts.fromBson<GetQuestionResponse>(responseString.Split((char)20)[0]);
            GetAnswersResponse responseAnswers = client.BsonConverts.fromBson<GetAnswersResponse>(responseString.Split((char)20)[1]);

            // remove the []
            responseAnswers.answers = responseAnswers.answers.Substring(1);
            responseAnswers.answers = responseAnswers.answers.Remove(responseAnswers.answers.Length - 1);

            return new Question{ question = responseQuestion.question, answers = responseAnswers.answers.Split(',').ToList() };
        }


        /*
            function update the screen according to the question
            input:
                none

            output:
                none
        */
        private void updateScreen(Question question)
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.answer1.BackColor = Color.Gray;
                this.answer2.BackColor = Color.Gray;
                this.answer3.BackColor = Color.Gray;
                this.answer4.BackColor = Color.Gray;

                this.questionLable.Text = question.question;
                this.answer1.Text = question.answers[0];
                this.answer2.Text = question.answers[1];
                this.answer3.Text = question.answers[2];
                this.answer4.Text = question.answers[3];
            });
        }


        private bool submitAnswer(uint answerIndex)
        {
            const int SUBMIT_ANSWER_CODE = 29;

            SubmitAnswerRequest request = new SubmitAnswerRequest { chosenAnswer = answerIndex.ToString() };

            this._socket.sendMsg((char)SUBMIT_ANSWER_CODE + client.BsonConverts.toBson(request));
            SubmitAnswerResponse response = client.BsonConverts.fromBson<SubmitAnswerResponse>(this._socket.getMsg());

            changeColorOfAnswer(Color.Red, this._chosenAnswer);
            changeColorOfAnswer(Color.Green, response.correctAnswerId);

            return answerIndex == response.correctAnswerId;
        }

        private void changeColorOfAnswer(Color color, uint buttonId)
        {
            this.Invoke((MethodInvoker)delegate
            {
                switch (buttonId)
                {
                    case 1:
                        this.answer1.BackColor = color;
                        break;

                    case 2:
                        this.answer2.BackColor = color;
                        break;

                    case 3:
                        this.answer3.BackColor = color;
                        break;

                    case 4:
                        this.answer4.BackColor = color;
                        break;

                    case 5:
                        // overtime
                        break;
                }
            });
        }

        private void circularProgressBar1_Click(object sender, EventArgs e)
        {
        }

        private void answer_click(Button button, uint buttonId)
        {
            if (!this._hasAnswerd && !this._isTimeOver)
            {
                this._hasAnswerd = true;
                this._chosenAnswer = buttonId;
                this.Invoke((MethodInvoker)delegate
                {
                    button.BackColor = Color.Blue;
                });
            }
        }

        private void answer1_Click(object sender, EventArgs e)
        {
            this.answer_click(this.answer1, 1);
        }

        private void answer2_Click(object sender, EventArgs e)
        {
            this.answer_click(this.answer2, 2);
        }

        private void answer3_Click(object sender, EventArgs e)
        {
            this.answer_click(this.answer3, 3);
        }

        private void answer4_Click(object sender, EventArgs e)
        {
            this.answer_click(this.answer4, 4);
        }

        void closer(object sender, FormClosingEventArgs e)
        {
            this.leaveGame();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.warningButtonNo.Visible = true;
            this.warningButtonYes.Visible = true;
            this.warningLabel1.Visible = true;
            this.warningLabel2.Visible = true;
            this.warningImg.Visible = true;
        }

        private void leaveGame()
        {
            const int GET_QUESTION_CODE = 25;
            string len = new string((char)0, 4);

            this._socket.sendMsg((char)GET_QUESTION_CODE + len);

            string responseString = this._socket.getMsg();

            this._communicatorThread.Abort();
            this._timeManagerThread.Abort();
        }

        private void warningButtonYes_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        class SubmitAnswerRequest
        {
            public string chosenAnswer;
        }

        class SubmitAnswerResponse
        {
            public uint status;
            public uint correctAnswerId;
        }

        class GetQuestionResponse
        {
            public string question;
            
        }

        class GetAnswersResponse
        {
            public string answers;
        }

        class Question
        {
            public string question;
            public List<string> answers;
        }

        private void warningButtonNo_Click(object sender, EventArgs e)
        {
            this.warningButtonNo.Visible = false;
            this.warningButtonYes.Visible = false;
            this.warningImg.Visible = false;
            this.warningLabel1.Visible = false;
            this.warningLabel2.Visible = false;
        }
    }
}
