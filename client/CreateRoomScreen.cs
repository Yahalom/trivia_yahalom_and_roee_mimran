﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class CreateRoomScreen : Form
    {
        private client.Socket _socket = null;

        public CreateRoomScreen(client.Socket socket)
        {
            this._socket = socket;
            this.BackgroundImage = client.Properties.Resources.mainMenuBackground;
            
            InitializeComponent();
            this.title.BackColor = System.Drawing.Color.Transparent;
            this.RoomName.BackColor = System.Drawing.Color.Transparent;
            this.timeOfQuestion.BackColor = System.Drawing.Color.Transparent;
            this.playersNumber.BackColor = System.Drawing.Color.Transparent;
            this.questionsNumber.BackColor = System.Drawing.Color.Transparent;

            this.RoomName.ForeColor = Color.GreenYellow;
            this.timeOfQuestion.ForeColor = Color.GreenYellow;
            this.playersNumber.ForeColor = Color.GreenYellow;
            this.questionsNumber.ForeColor = Color.GreenYellow;

            this.Create.BackColor = System.Drawing.Color.Transparent;
            this.Back.BackColor = System.Drawing.Color.Transparent;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void RoomName_Click(object sender, EventArgs e)
        {

        }

        private void RoomNameTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void Create_Click(object sender, EventArgs e)
        {
            const int CODE = 16;
            const int ERROR = 0;

            if(!this.checkForValid(this.playersNumberTextBox.Text, this.questionsNumberTextBox.Text, this.timeOfQuestionTextBox.Text))
            {
                new Exception.Exception("invalid input").ShowDialog();
                return;
            }

            var request = new CreateRoomRequest
            {
                roomName = this.RoomNameTextBox.Text,
                maxUsers = this.playersNumberTextBox.Text,
                questionCount = this.questionsNumberTextBox.Text,
                answerTimeout = this.timeOfQuestionTextBox.Text
            };
            this._socket.sendMsg((char)CODE + client.BsonConverts.toBson(request));

            // get the response from the server
            string response = this._socket.getMsg();

            // decode it to string and print the status
            CreateRoomResponse status = client.BsonConverts.fromBson<CreateRoomResponse>(response);

            if (status.status == ERROR)
            {
                new Exception.Exception("Couldn't Create Room").ShowDialog();
            }
            else
            {
                this.Visible = false;
                new Menu.ShowPlayersScreen(this._socket, status.roomId, true).ShowDialog();
            }
            this.Close();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkForValid(string maxUsers, string questionCount, string answerTimeout)
        {
            if(maxUsers.Length > 0 && questionCount.Length > 0 && answerTimeout.Length > 0)
            {
                if(isNumeric(maxUsers) && isNumeric(questionCount) && isNumeric(answerTimeout))
                {
                    return true;
                }
            }
            return false;
        }


        // function was taken from https://stackoverflow.com/questions/22794356/isnumeric-function-in-c-sharp/22794586
        public bool isNumeric(string value)
        {
            return value.All(char.IsNumber);
        }



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void title_Click(object sender, EventArgs e)
        {

        }

        private void playersNumberTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void timeOfQuestion_Click(object sender, EventArgs e)
        {

        }

        private void playersNumber_Click(object sender, EventArgs e)
        {

        }

        private void timeOfQuestionTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }

    class CreateRoomRequest
    {
        public string roomName;
        public string maxUsers;
        public string questionCount;
        public string answerTimeout;
    }

    class CreateRoomResponse
    {
        public uint status;
        public string roomId;
    }
}
