﻿namespace login
{
    partial class LoginScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginScreen));
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.usernameLable = new System.Windows.Forms.Label();
            this.passwordLable = new System.Windows.Forms.Label();
            this.signup = new System.Windows.Forms.Label();
            this.signupButton = new System.Windows.Forms.Label();
            this.login = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.login)).BeginInit();
            this.SuspendLayout();
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(343, 187);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(187, 20);
            this.usernameTextBox.TabIndex = 0;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(343, 293);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(187, 20);
            this.passwordTextBox.TabIndex = 1;
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            // 
            // usernameLable
            // 
            this.usernameLable.AutoSize = true;
            this.usernameLable.BackColor = System.Drawing.SystemColors.Window;
            this.usernameLable.Font = new System.Drawing.Font("Snap ITC", 30F);
            this.usernameLable.Location = new System.Drawing.Point(49, 161);
            this.usernameLable.Name = "usernameLable";
            this.usernameLable.Size = new System.Drawing.Size(253, 51);
            this.usernameLable.TabIndex = 2;
            this.usernameLable.Text = "Username:";
            this.usernameLable.Click += new System.EventHandler(this.usernameLable_Click);
            // 
            // passwordLable
            // 
            this.passwordLable.AutoSize = true;
            this.passwordLable.Font = new System.Drawing.Font("Snap ITC", 30F);
            this.passwordLable.Location = new System.Drawing.Point(49, 267);
            this.passwordLable.Name = "passwordLable";
            this.passwordLable.Size = new System.Drawing.Size(255, 51);
            this.passwordLable.TabIndex = 3;
            this.passwordLable.Text = "Password:";
            // 
            // signup
            // 
            this.signup.AutoSize = true;
            this.signup.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.signup.Location = new System.Drawing.Point(190, 351);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(280, 27);
            this.signup.TabIndex = 6;
            this.signup.Text = "Don\'t have an account?";
            // 
            // signupButton
            // 
            this.signupButton.AutoSize = true;
            this.signupButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signupButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.signupButton.Location = new System.Drawing.Point(476, 351);
            this.signupButton.Name = "signupButton";
            this.signupButton.Size = new System.Drawing.Size(99, 27);
            this.signupButton.TabIndex = 6;
            this.signupButton.Text = "Sign up";
            this.signupButton.Click += new System.EventHandler(this.signupButton_Click_1);
            // 
            // login
            // 
            this.login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.login.Image = ((System.Drawing.Image)(resources.GetObject("login.Image")));
            this.login.Location = new System.Drawing.Point(255, 381);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(215, 38);
            this.login.TabIndex = 7;
            this.login.TabStop = false;
            this.login.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // LoginScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 492);
            this.Controls.Add(this.login);
            this.Controls.Add(this.signupButton);
            this.Controls.Add(this.signup);
            this.Controls.Add(this.passwordLable);
            this.Controls.Add(this.usernameLable);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.usernameTextBox);
            this.Name = "LoginScreen";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.login)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label usernameLable;
        private System.Windows.Forms.Label passwordLable;
        private System.Windows.Forms.Label signup;
        private System.Windows.Forms.Label signupButton;
        private System.Windows.Forms.PictureBox login;
    }
}

