﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;


namespace Menu
{
    public partial class statistics : Form
    {
        private client.Socket _socket = null;

        public statistics(client.Socket socket)
        {
            this.BackgroundImage = client.Properties.Resources.mainMenuBackground;
            this._socket = socket;

            InitializeComponent();

            this.highScore.BackColor = System.Drawing.Color.Transparent;
            this.myScore.BackColor = System.Drawing.Color.Transparent;
            this.back.BackColor = System.Drawing.Color.Transparent;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void myScore_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            new Menu.MyScoreScreen(this._socket).ShowDialog();
            this.Visible = true;
        }

        private void highScore_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            new Menu.TopScoreScreen(this._socket).ShowDialog();
            this.Visible = true;
        }
    }

}
