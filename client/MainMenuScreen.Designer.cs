﻿namespace Menu
{
    partial class MainMenuScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuLable = new System.Windows.Forms.Label();
            this.createRoomButton = new System.Windows.Forms.PictureBox();
            this.statisticsButton = new System.Windows.Forms.PictureBox();
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.joinRoomButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.createRoomButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statisticsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinRoomButton)).BeginInit();
            this.SuspendLayout();
            // 
            // menuLable
            // 
            this.menuLable.AutoSize = true;
            this.menuLable.Font = new System.Drawing.Font("Snap ITC", 40F);
            this.menuLable.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.menuLable.Location = new System.Drawing.Point(346, 24);
            this.menuLable.Name = "menuLable";
            this.menuLable.Size = new System.Drawing.Size(193, 69);
            this.menuLable.TabIndex = 3;
            this.menuLable.Text = "Menu";
            this.menuLable.Click += new System.EventHandler(this.passwordLable_Click);
            // 
            // createRoomButton
            // 
            this.createRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createRoomButton.Image = global::client.Properties.Resources.createRoom;
            this.createRoomButton.Location = new System.Drawing.Point(12, 135);
            this.createRoomButton.Name = "createRoomButton";
            this.createRoomButton.Size = new System.Drawing.Size(361, 125);
            this.createRoomButton.TabIndex = 4;
            this.createRoomButton.TabStop = false;
            this.createRoomButton.Click += new System.EventHandler(this.createRoomButton_Click);
            // 
            // statisticsButton
            // 
            this.statisticsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.statisticsButton.Image = global::client.Properties.Resources.statisitcs;
            this.statisticsButton.Location = new System.Drawing.Point(12, 415);
            this.statisticsButton.Name = "statisticsButton";
            this.statisticsButton.Size = new System.Drawing.Size(361, 124);
            this.statisticsButton.TabIndex = 4;
            this.statisticsButton.TabStop = false;
            this.statisticsButton.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // exitButton
            // 
            this.exitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitButton.Image = global::client.Properties.Resources.signout;
            this.exitButton.Location = new System.Drawing.Point(497, 415);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(360, 124);
            this.exitButton.TabIndex = 7;
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // joinRoomButton
            // 
            this.joinRoomButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.joinRoomButton.Image = global::client.Properties.Resources.joinRoom;
            this.joinRoomButton.Location = new System.Drawing.Point(497, 135);
            this.joinRoomButton.Name = "joinRoomButton";
            this.joinRoomButton.Size = new System.Drawing.Size(360, 125);
            this.joinRoomButton.TabIndex = 5;
            this.joinRoomButton.TabStop = false;
            this.joinRoomButton.Click += new System.EventHandler(this.joinRoomButton_Click);
            // 
            // MainMenuScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 615);
            this.Controls.Add(this.createRoomButton);
            this.Controls.Add(this.menuLable);
            this.Controls.Add(this.statisticsButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.joinRoomButton);
            this.Name = "MainMenuScreen";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.createRoomButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statisticsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinRoomButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label menuLable;
        private System.Windows.Forms.PictureBox createRoomButton;
        private System.Windows.Forms.PictureBox joinRoomButton;
        private System.Windows.Forms.PictureBox exitButton;
        private System.Windows.Forms.PictureBox statisticsButton;
    }
}

