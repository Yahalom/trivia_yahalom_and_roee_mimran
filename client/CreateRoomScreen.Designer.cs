﻿namespace Menu
{
    partial class CreateRoomScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title = new System.Windows.Forms.Label();
            this.playersNumberTextBox = new System.Windows.Forms.TextBox();
            this.RoomName = new System.Windows.Forms.Label();
            this.timeOfQuestion = new System.Windows.Forms.Label();
            this.playersNumber = new System.Windows.Forms.Label();
            this.timeOfQuestionTextBox = new System.Windows.Forms.TextBox();
            this.RoomNameTextBox = new System.Windows.Forms.TextBox();
            this.Create = new System.Windows.Forms.PictureBox();
            this.Back = new System.Windows.Forms.PictureBox();
            this.questionsNumberTextBox = new System.Windows.Forms.TextBox();
            this.questionsNumber = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Create)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Back)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Snap ITC", 20.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.title.Location = new System.Drawing.Point(340, 24);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(208, 35);
            this.title.TabIndex = 0;
            this.title.Text = "Create Room";
            this.title.Click += new System.EventHandler(this.title_Click);
            // 
            // playersNumberTextBox
            // 
            this.playersNumberTextBox.BackColor = System.Drawing.SystemColors.HotTrack;
            this.playersNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playersNumberTextBox.Location = new System.Drawing.Point(455, 270);
            this.playersNumberTextBox.Name = "playersNumberTextBox";
            this.playersNumberTextBox.Size = new System.Drawing.Size(173, 29);
            this.playersNumberTextBox.TabIndex = 1;
            this.playersNumberTextBox.TextChanged += new System.EventHandler(this.playersNumberTextBox_TextChanged);
            // 
            // RoomName
            // 
            this.RoomName.AutoSize = true;
            this.RoomName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.RoomName.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.RoomName.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.RoomName.Location = new System.Drawing.Point(202, 161);
            this.RoomName.Name = "RoomName";
            this.RoomName.Size = new System.Drawing.Size(200, 35);
            this.RoomName.TabIndex = 2;
            this.RoomName.Text = "Room\'s name";
            this.RoomName.Click += new System.EventHandler(this.RoomName_Click);
            // 
            // timeOfQuestion
            // 
            this.timeOfQuestion.AutoSize = true;
            this.timeOfQuestion.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.timeOfQuestion.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.timeOfQuestion.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.timeOfQuestion.Location = new System.Drawing.Point(169, 209);
            this.timeOfQuestion.Name = "timeOfQuestion";
            this.timeOfQuestion.Size = new System.Drawing.Size(287, 35);
            this.timeOfQuestion.TabIndex = 2;
            this.timeOfQuestion.Text = "time per question";
            this.timeOfQuestion.Click += new System.EventHandler(this.timeOfQuestion_Click);
            // 
            // playersNumber
            // 
            this.playersNumber.AutoSize = true;
            this.playersNumber.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.playersNumber.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.playersNumber.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.playersNumber.Location = new System.Drawing.Point(159, 264);
            this.playersNumber.Name = "playersNumber";
            this.playersNumber.Size = new System.Drawing.Size(290, 35);
            this.playersNumber.TabIndex = 2;
            this.playersNumber.Text = "number of players";
            this.playersNumber.Click += new System.EventHandler(this.playersNumber_Click);
            // 
            // timeOfQuestionTextBox
            // 
            this.timeOfQuestionTextBox.BackColor = System.Drawing.SystemColors.HotTrack;
            this.timeOfQuestionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timeOfQuestionTextBox.Location = new System.Drawing.Point(455, 215);
            this.timeOfQuestionTextBox.Name = "timeOfQuestionTextBox";
            this.timeOfQuestionTextBox.Size = new System.Drawing.Size(173, 29);
            this.timeOfQuestionTextBox.TabIndex = 1;
            this.timeOfQuestionTextBox.TextChanged += new System.EventHandler(this.timeOfQuestionTextBox_TextChanged);
            // 
            // RoomNameTextBox
            // 
            this.RoomNameTextBox.BackColor = System.Drawing.SystemColors.HotTrack;
            this.RoomNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.5F);
            this.RoomNameTextBox.Location = new System.Drawing.Point(455, 167);
            this.RoomNameTextBox.Name = "RoomNameTextBox";
            this.RoomNameTextBox.Size = new System.Drawing.Size(173, 29);
            this.RoomNameTextBox.TabIndex = 1;
            this.RoomNameTextBox.TextChanged += new System.EventHandler(this.RoomNameTextBox_TextChanged);
            // 
            // Create
            // 
            this.Create.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Create.Image = global::client.Properties.Resources.create;
            this.Create.Location = new System.Drawing.Point(470, 383);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(280, 99);
            this.Create.TabIndex = 3;
            this.Create.TabStop = false;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // Back
            // 
            this.Back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Back.Image = global::client.Properties.Resources.back;
            this.Back.Location = new System.Drawing.Point(122, 383);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(280, 99);
            this.Back.TabIndex = 3;
            this.Back.TabStop = false;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // questionsNumberTextBox
            // 
            this.questionsNumberTextBox.BackColor = System.Drawing.SystemColors.HotTrack;
            this.questionsNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionsNumberTextBox.Location = new System.Drawing.Point(455, 320);
            this.questionsNumberTextBox.Name = "questionsNumberTextBox";
            this.questionsNumberTextBox.Size = new System.Drawing.Size(173, 29);
            this.questionsNumberTextBox.TabIndex = 1;
            this.questionsNumberTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // questionsNumber
            // 
            this.questionsNumber.AutoSize = true;
            this.questionsNumber.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.questionsNumber.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.questionsNumber.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.questionsNumber.Location = new System.Drawing.Point(124, 314);
            this.questionsNumber.Name = "questionsNumber";
            this.questionsNumber.Size = new System.Drawing.Size(325, 35);
            this.questionsNumber.TabIndex = 2;
            this.questionsNumber.Text = "number of questions";
            this.questionsNumber.Click += new System.EventHandler(this.label1_Click);
            // 
            // CreateRoomScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 615);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.questionsNumber);
            this.Controls.Add(this.playersNumber);
            this.Controls.Add(this.timeOfQuestion);
            this.Controls.Add(this.RoomName);
            this.Controls.Add(this.RoomNameTextBox);
            this.Controls.Add(this.timeOfQuestionTextBox);
            this.Controls.Add(this.questionsNumberTextBox);
            this.Controls.Add(this.playersNumberTextBox);
            this.Controls.Add(this.title);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "CreateRoomScreen";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Create)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Back)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TextBox playersNumberTextBox;
        private System.Windows.Forms.Label RoomName;
        private System.Windows.Forms.Label timeOfQuestion;
        private System.Windows.Forms.Label playersNumber;
        private System.Windows.Forms.TextBox timeOfQuestionTextBox;
        private System.Windows.Forms.TextBox RoomNameTextBox;
        private System.Windows.Forms.PictureBox Create;
        private System.Windows.Forms.PictureBox Back;
        private System.Windows.Forms.TextBox questionsNumberTextBox;
        private System.Windows.Forms.Label questionsNumber;
    }
}

