﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace client
{
    public class Socket
    {
        private NetworkStream clientStream;

        public Socket()
        {
            TcpClient clientSocket = new TcpClient();
            IPEndPoint serverEnePoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 65432);
            clientSocket.Connect(serverEnePoint);
            this.clientStream = clientSocket.GetStream();
        }

        public void sendMsg(string msg)
        {
            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(msg);
                this.clientStream.Write(buffer, 0, buffer.Length);
                this.clientStream.Flush();
            }
            catch
            {
                new Exception.Exception("Communication Error").ShowDialog();
                System.Environment.Exit(1);
            }
        }

        public string getMsg()
        {
            try
            {
                byte[] buffer = new byte[4096];
                this.clientStream.Read(buffer, 0, 4096);
                return Encoding.Default.GetString(buffer);
            }
            catch
            {
                new Exception.Exception("Communication Error").ShowDialog();
                System.Environment.Exit(1);
                return "";
            }
        }
    }
}
