﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Threading;


namespace Menu
{
    public partial class ShowPlayersScreen : Form
    {
        private client.Socket _socket = null;

        private string _roomId = "0";

        private bool _isAdmin = false;
        private bool _isStillWaiting = true;
        private bool _hasGameBegun = false;

        private Mutex _mutex = new Mutex();

        GetRoomStateResponse _roomState;
        private Thread _communicatorThread;
        private Thread _startGameThread;

        public ShowPlayersScreen(client.Socket socket, string roomId, bool isAdmin)
        {
            this._socket = socket;
            this._roomId = roomId;
            this._isAdmin = isAdmin;

            InitializeComponent();

            this.startButton.BackColor = System.Drawing.Color.Transparent;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // checking if the current pclayer is the admin so he will be able to start the game
            if(!this._isAdmin)
            {
                this.startButton.Visible = false;
            }

            this._startGameThread = new Thread(startGame);
            this._communicatorThread = new Thread(Communicator);
            this._communicatorThread.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            const int LEAVE_ROOM_CODE = 25;
            const int CLOSE_ROOM_CODE = 19;
            string len = new string((char)0, 4);

            this._mutex.WaitOne();

            if (this._isAdmin)
            {
                this._socket.sendMsg((char)CLOSE_ROOM_CODE + len);
            }

            this._socket.sendMsg((char)LEAVE_ROOM_CODE + len);
            this._socket.getMsg();

            this._communicatorThread.Abort();

            this._mutex.ReleaseMutex();
            this.Close();
        }

        private void Communicator()
        {
            const int GET_ROOM_STATE_REQUEST_CODE = 23;
            const int GET_ROOM_STATE_RESPONSE_CODE = 24;
            const int LEAVE_ROOM_RESPSONE_CODE = 26;
            const int LEAVE_ROOM_CODE = 25;


            GetRoomStateResponse response = new GetRoomStateResponse();
            
            while (this._isStillWaiting)
            {
                this._mutex.WaitOne();
                // ask from the server the player in the room
                string len = new string((char)0, 4);
                this._socket.sendMsg((char)GET_ROOM_STATE_REQUEST_CODE + len);

                String msg = this._socket.getMsg();
                this._mutex.ReleaseMutex();

                switch (msg[0])
                {
                    case (char)GET_ROOM_STATE_RESPONSE_CODE:
                        this._roomState = client.BsonConverts.fromBson<GetRoomStateResponse>(msg);
                        if (this.IsHandleCreated)
                        {
                            this.updateData(this._roomState);
                        }
                        break;

                    case (char)LEAVE_ROOM_RESPSONE_CODE:
                        if (!this._isAdmin)
                        {
                            this._socket.sendMsg((char)LEAVE_ROOM_CODE + len);
                            this._socket.getMsg();
                            this.closeWindow();
                            new Exception.Exception("the admin left the room").ShowDialog();
                        }
                        break;
                }

                if (this._hasGameBegun && !this._isAdmin)
                {
                    this._isStillWaiting = false;
                    startGame();
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

            this.closeWindow();
        }

        private void closeWindow()
        {
            while(true)
            {
                if(this.IsHandleCreated)
                {
                    break;
                }
            }

            this.Invoke((MethodInvoker)delegate
            {
                // close the form on the forms thread
                this.Close();
            });
        }

        private void updateData(GetRoomStateResponse response)
        {
            List<Player> players = new List<Player>();

            // remove the []
            response.players = response.players.Substring(1);
            response.players = response.players.Remove(response.players.Length - 1);

            foreach (var user in response.players.Split(',').ToList())
            {
                players.Add(new Player { name = user, isAdmin = false });
            }
            players[0].isAdmin = true;

            try
            {
                this.QuestionCount.Invoke((MethodInvoker)delegate
                {
                    this.QuestionCount.Text = "Question Number: " + response.questionCount;
                });
                // update the data

                this.TimePerQuestion.Invoke((MethodInvoker)delegate
                {
                    this.TimePerQuestion.Text = "Time Per Question: " + response.timePerQuestion;
                });

                this.dataGridView1.Invoke((MethodInvoker)delegate
                {
                    this.dataGridView1.DataSource = null;
                    this.dataGridView1.DataSource = players;
                });
            }
            catch (System.Exception) { }
            
            this._hasGameBegun = response.hasGameBegun;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            startGame();
        }

        private void startGame()
        {
            const int START_GAME_CODE = 21;
            const int START_GAME_RESPONSE_CODE = 22;
            string len = new string((char)0, 4);

            this._mutex.WaitOne();
            this._socket.sendMsg((char)START_GAME_CODE + len);
            this._mutex.ReleaseMutex();

            while (true)
            {
                this._mutex.WaitOne();

                if (this._socket.getMsg()[0] == (char)START_GAME_RESPONSE_CODE)
                {
                    break;
                }

                this._mutex.ReleaseMutex();
            }

            this._isStillWaiting = false;

            this.Invoke((MethodInvoker)delegate
            {
                this.Visible = false;
                new Game.GameScreen(this._socket, this._roomState).ShowDialog();
                this.Visible = true;
            });
        }
    }

    class GetPlayersInRoomRequest
    {
        public string roomId;
    }

    public class GetRoomStateResponse
    {
        public uint status;
        public bool hasGameBegun;
        public string players;
        public uint questionCount;
        public uint timePerQuestion;

    }

    class Player
    {
        public string name { get; set; }
        public bool isAdmin { get; set; }
    }

    class LeaveRoomRequest
    {
        public string roomId;
    }
}
