﻿namespace Game
{
    partial class GameScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.questionLable = new System.Windows.Forms.Label();
            this.timer = new CircularProgressBar.CircularProgressBar();
            this.cover = new System.Windows.Forms.Label();
            this.waitSen1 = new System.Windows.Forms.Label();
            this.waitSen2 = new System.Windows.Forms.Label();
            this.waitSen3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.answer4 = new System.Windows.Forms.Button();
            this.answer3 = new System.Windows.Forms.Button();
            this.answer2 = new System.Windows.Forms.Button();
            this.answer1 = new System.Windows.Forms.Button();
            this.waitingPic = new System.Windows.Forms.PictureBox();
            this.warningButtonNo = new System.Windows.Forms.Button();
            this.warningButtonYes = new System.Windows.Forms.Button();
            this.warningLabel2 = new System.Windows.Forms.Label();
            this.warningLabel1 = new System.Windows.Forms.Label();
            this.warningImg = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.warningImg)).BeginInit();
            this.SuspendLayout();
            // 
            // questionLable
            // 
            this.questionLable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.questionLable.Cursor = System.Windows.Forms.Cursors.Default;
            this.questionLable.Dock = System.Windows.Forms.DockStyle.Top;
            this.questionLable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.questionLable.Font = new System.Drawing.Font("Britannic Bold", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questionLable.ForeColor = System.Drawing.Color.White;
            this.questionLable.Location = new System.Drawing.Point(0, 0);
            this.questionLable.Name = "questionLable";
            this.questionLable.Size = new System.Drawing.Size(859, 151);
            this.questionLable.TabIndex = 0;
            this.questionLable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer
            // 
            this.timer.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.timer.AnimationSpeed = 500;
            this.timer.BackColor = System.Drawing.Color.Transparent;
            this.timer.Cursor = System.Windows.Forms.Cursors.Default;
            this.timer.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer.ForeColor = System.Drawing.Color.White;
            this.timer.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.timer.InnerMargin = 2;
            this.timer.InnerWidth = -1;
            this.timer.Location = new System.Drawing.Point(359, 242);
            this.timer.MarqueeAnimationSpeed = 2000;
            this.timer.Name = "timer";
            this.timer.OuterColor = System.Drawing.Color.Gray;
            this.timer.OuterMargin = -25;
            this.timer.OuterWidth = 26;
            this.timer.ProgressColor = System.Drawing.Color.Red;
            this.timer.ProgressWidth = 12;
            this.timer.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.timer.Size = new System.Drawing.Size(125, 108);
            this.timer.StartAngle = 270;
            this.timer.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.timer.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.timer.SubscriptText = "";
            this.timer.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.timer.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.timer.SuperscriptText = "";
            this.timer.TabIndex = 3;
            this.timer.TextMargin = new System.Windows.Forms.Padding(0);
            this.timer.Value = 100;
            this.timer.Click += new System.EventHandler(this.circularProgressBar1_Click);
            // 
            // cover
            // 
            this.cover.Location = new System.Drawing.Point(-11, -7);
            this.cover.Name = "cover";
            this.cover.Size = new System.Drawing.Size(364, 462);
            this.cover.TabIndex = 5;
            // 
            // waitSen1
            // 
            this.waitSen1.AutoSize = true;
            this.waitSen1.Font = new System.Drawing.Font("Gill Sans Ultra Bold Condensed", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitSen1.Location = new System.Drawing.Point(38, 21);
            this.waitSen1.Name = "waitSen1";
            this.waitSen1.Size = new System.Drawing.Size(250, 88);
            this.waitSen1.TabIndex = 6;
            this.waitSen1.Text = "Waiting";
            // 
            // waitSen2
            // 
            this.waitSen2.AutoSize = true;
            this.waitSen2.Font = new System.Drawing.Font("Gill Sans Ultra Bold Condensed", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitSen2.Location = new System.Drawing.Point(38, 151);
            this.waitSen2.Name = "waitSen2";
            this.waitSen2.Size = new System.Drawing.Size(293, 88);
            this.waitSen2.TabIndex = 6;
            this.waitSen2.Text = "for other";
            // 
            // waitSen3
            // 
            this.waitSen3.AutoSize = true;
            this.waitSen3.Font = new System.Drawing.Font("Gill Sans Ultra Bold Condensed", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitSen3.Location = new System.Drawing.Point(38, 295);
            this.waitSen3.Name = "waitSen3";
            this.waitSen3.Size = new System.Drawing.Size(240, 88);
            this.waitSen3.TabIndex = 6;
            this.waitSen3.Text = "players";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::client.Properties.Resources.backArrow;
            this.pictureBox1.Location = new System.Drawing.Point(407, 422);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(31, 33);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // answer4
            // 
            this.answer4.BackColor = System.Drawing.Color.Gray;
            this.answer4.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answer4.ForeColor = System.Drawing.Color.White;
            this.answer4.Image = global::client.Properties.Resources._4;
            this.answer4.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.answer4.Location = new System.Drawing.Point(490, 309);
            this.answer4.Name = "answer4";
            this.answer4.Size = new System.Drawing.Size(357, 105);
            this.answer4.TabIndex = 2;
            this.answer4.UseVisualStyleBackColor = false;
            this.answer4.Click += new System.EventHandler(this.answer4_Click);
            // 
            // answer3
            // 
            this.answer3.BackColor = System.Drawing.Color.Gray;
            this.answer3.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answer3.ForeColor = System.Drawing.Color.White;
            this.answer3.Image = global::client.Properties.Resources._3;
            this.answer3.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.answer3.Location = new System.Drawing.Point(6, 309);
            this.answer3.Name = "answer3";
            this.answer3.Size = new System.Drawing.Size(347, 105);
            this.answer3.TabIndex = 2;
            this.answer3.UseVisualStyleBackColor = false;
            this.answer3.Click += new System.EventHandler(this.answer3_Click);
            // 
            // answer2
            // 
            this.answer2.BackColor = System.Drawing.Color.Gray;
            this.answer2.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answer2.ForeColor = System.Drawing.Color.White;
            this.answer2.Image = global::client.Properties.Resources._2;
            this.answer2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.answer2.Location = new System.Drawing.Point(490, 170);
            this.answer2.Name = "answer2";
            this.answer2.Size = new System.Drawing.Size(357, 105);
            this.answer2.TabIndex = 2;
            this.answer2.UseVisualStyleBackColor = false;
            this.answer2.Click += new System.EventHandler(this.answer2_Click);
            // 
            // answer1
            // 
            this.answer1.BackColor = System.Drawing.Color.Gray;
            this.answer1.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answer1.ForeColor = System.Drawing.Color.White;
            this.answer1.Image = global::client.Properties.Resources._1;
            this.answer1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.answer1.Location = new System.Drawing.Point(6, 170);
            this.answer1.Name = "answer1";
            this.answer1.Size = new System.Drawing.Size(347, 105);
            this.answer1.TabIndex = 2;
            this.answer1.UseVisualStyleBackColor = false;
            this.answer1.Click += new System.EventHandler(this.answer1_Click);
            // 
            // waitingPic
            // 
            this.waitingPic.Image = global::client.Properties.Resources.waiting;
            this.waitingPic.Location = new System.Drawing.Point(308, -30);
            this.waitingPic.Name = "waitingPic";
            this.waitingPic.Size = new System.Drawing.Size(557, 485);
            this.waitingPic.TabIndex = 4;
            this.waitingPic.TabStop = false;
            // 
            // warningButtonNo
            // 
            this.warningButtonNo.BackColor = System.Drawing.Color.Red;
            this.warningButtonNo.Location = new System.Drawing.Point(440, 266);
            this.warningButtonNo.Name = "warningButtonNo";
            this.warningButtonNo.Size = new System.Drawing.Size(90, 36);
            this.warningButtonNo.TabIndex = 11;
            this.warningButtonNo.Text = "No";
            this.warningButtonNo.UseVisualStyleBackColor = false;
            this.warningButtonNo.Visible = false;
            this.warningButtonNo.Click += new System.EventHandler(this.warningButtonNo_Click);
            // 
            // warningButtonYes
            // 
            this.warningButtonYes.BackColor = System.Drawing.Color.Lime;
            this.warningButtonYes.Location = new System.Drawing.Point(319, 266);
            this.warningButtonYes.Name = "warningButtonYes";
            this.warningButtonYes.Size = new System.Drawing.Size(90, 36);
            this.warningButtonYes.TabIndex = 12;
            this.warningButtonYes.Text = "Yes";
            this.warningButtonYes.UseVisualStyleBackColor = false;
            this.warningButtonYes.Visible = false;
            this.warningButtonYes.Click += new System.EventHandler(this.warningButtonYes_Click);
            // 
            // warningLabel2
            // 
            this.warningLabel2.AutoSize = true;
            this.warningLabel2.BackColor = System.Drawing.Color.Gold;
            this.warningLabel2.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLabel2.Location = new System.Drawing.Point(326, 229);
            this.warningLabel2.Name = "warningLabel2";
            this.warningLabel2.Size = new System.Drawing.Size(204, 22);
            this.warningLabel2.TabIndex = 9;
            this.warningLabel2.Text = "you want to exit?";
            this.warningLabel2.Visible = false;
            // 
            // warningLabel1
            // 
            this.warningLabel1.AutoSize = true;
            this.warningLabel1.BackColor = System.Drawing.Color.Gold;
            this.warningLabel1.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLabel1.Location = new System.Drawing.Point(351, 195);
            this.warningLabel1.Name = "warningLabel1";
            this.warningLabel1.Size = new System.Drawing.Size(148, 22);
            this.warningLabel1.TabIndex = 10;
            this.warningLabel1.Text = "Are you sure";
            this.warningLabel1.Visible = false;
            // 
            // warningImg
            // 
            this.warningImg.Image = global::client.Properties.Resources.warning;
            this.warningImg.Location = new System.Drawing.Point(267, 107);
            this.warningImg.Name = "warningImg";
            this.warningImg.Size = new System.Drawing.Size(325, 252);
            this.warningImg.TabIndex = 8;
            this.warningImg.TabStop = false;
            this.warningImg.Visible = false;
            // 
            // GameScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(859, 467);
            this.Controls.Add(this.waitSen3);
            this.Controls.Add(this.waitSen2);
            this.Controls.Add(this.waitSen1);
            this.Controls.Add(this.cover);
            this.Controls.Add(this.waitingPic);
            this.Controls.Add(this.warningButtonNo);
            this.Controls.Add(this.warningButtonYes);
            this.Controls.Add(this.warningLabel2);
            this.Controls.Add(this.warningLabel1);
            this.Controls.Add(this.warningImg);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.timer);
            this.Controls.Add(this.answer4);
            this.Controls.Add(this.answer3);
            this.Controls.Add(this.answer2);
            this.Controls.Add(this.answer1);
            this.Controls.Add(this.questionLable);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "GameScreen";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.warningImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label questionLable;
        private System.Windows.Forms.Button answer1;
        private System.Windows.Forms.Button answer2;
        private System.Windows.Forms.Button answer3;
        private System.Windows.Forms.Button answer4;
        private CircularProgressBar.CircularProgressBar timer;
        private System.Windows.Forms.PictureBox waitingPic;
        private System.Windows.Forms.Label cover;
        private System.Windows.Forms.Label waitSen1;
        private System.Windows.Forms.Label waitSen2;
        private System.Windows.Forms.Label waitSen3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button warningButtonNo;
        private System.Windows.Forms.Button warningButtonYes;
        private System.Windows.Forms.Label warningLabel2;
        private System.Windows.Forms.Label warningLabel1;
        private System.Windows.Forms.PictureBox warningImg;
    }
}

