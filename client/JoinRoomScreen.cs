﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;


namespace Menu
{
    public partial class JoinRoomScreen : Form
    {
        const int ID = 1;
        const int NAME = 0;

        private client.Socket _socket = null;
        private List<string> _rooms = new List<string>();

        public JoinRoomScreen(client.Socket socket)
        {
            this._socket = socket;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            const int CODE = 12;

            string len = new string((char)0, 4);
            this._socket.sendMsg(((char)CODE).ToString() + len);

            // get the response from the server
            // decode it to string and print the status
            GetRoomsResponse response = client.BsonConverts.fromBson<GetRoomsResponse>(this._socket.getMsg());
            List<Room> rooms = new List<Room>();

            try
            {
                // remove the []
                response.rooms = response.rooms.Substring(1);
                response.rooms = response.rooms.Remove(response.rooms.Length - 1);

                this._rooms = response.rooms.Split(',').ToList();

                foreach (var room in this._rooms)
                {
                    var nameAndId = room.Split(':').ToList();
                    rooms.Add(new Room { Name = nameAndId[NAME], id = nameAndId[ID] });
                }

                this.dataGridView1.AutoSize = true;
                this.dataGridView1.DataSource = rooms;

                // Initialize the form.
                this.Controls.Add(this.dataGridView1);
                this.AutoSize = false;
            }
            catch
            {
                this.Visible = false;
                new Exception.Exception("No Rooms Available").ShowDialog();
                this.Close();
            }
        }

        // join room function
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // extract the id from the room we want to enter to
            string id = this._rooms[e.RowIndex].Split(':').ToList()[ID];

            this.Visible = false;

            if (this.joinRoom(id))
            {
                // show an error screen
                new Exception.Exception("Couldn't enter to the room").ShowDialog();
            }
            else
            {
                // show the players of the current room
                new Menu.ShowPlayersScreen(this._socket, id, false).ShowDialog();
            }
        }

        /*
         * function join a room according to the wanted id
         */
        private bool joinRoom(string id)
        {
            const int CODE = 15;

            // send the msg and get the repsonse
            JoinRoomRequest request = new JoinRoomRequest { roomId = id };
            this._socket.sendMsg(((char)CODE).ToString() + client.BsonConverts.toBson(request));
            JoinRoomResponse repsonse = client.BsonConverts.fromBson<JoinRoomResponse>(this._socket.getMsg());

            // check for validtion
            return repsonse.status == null;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    class GetRoomsResponse
    {
        public uint status;
        public string rooms;
    }

    class Room
    {
        public string Name { get; set; }
        public string id;
    }

    class JoinRoomRequest
    {
        public string roomId;
    }

    class JoinRoomResponse
    {
        public string status;
    }
}
