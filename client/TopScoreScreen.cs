﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;


namespace Menu
{
    public partial class TopScoreScreen : Form
    {
        private client.Socket _socket = null;

        public TopScoreScreen(client.Socket socket)
        {
            this._socket = socket;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            const int CODE = 17;
            const int NAME = 6;
            const int WON_GAMES = 4;
            const string ERROR = "0";

            string len = new string((char)0, 4);
            this._socket.sendMsg(((char)CODE).ToString() + len);

            // get the response from the server
            GetTopScoreResponse response = client.BsonConverts.fromBson<GetTopScoreResponse>(this._socket.getMsg());
            var players = response.topScore.Split('&').ToList();


            try
            {
                this.WonGames1.Text = players[0].Split(',').ToList()[WON_GAMES];
                this.Name1.Text = players[0].Split(',').ToList()[NAME];

                this.WonGames2.Text = players[1].Split(',').ToList()[WON_GAMES];
                this.Name2.Text = players[1].Split(',').ToList()[NAME];

                this.WonGames3.Text = players[2].Split(',').ToList()[WON_GAMES];
                this.Name3.Text = players[2].Split(',').ToList()[NAME];
            }
            catch
            {
            }
            
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }

    class GetTopScoreResponse
    {
        public string topScore;
    }
}
