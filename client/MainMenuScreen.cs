﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class MainMenuScreen : Form
    {
        private client.Socket _socket = null;

        public MainMenuScreen(client.Socket socket)
        {
            this._socket = socket;
            this.BackgroundImage = client.Properties.Resources.mainMenuBackground;
            
            InitializeComponent();

            this.createRoomButton.BackColor = System.Drawing.Color.Transparent;
            this.joinRoomButton.BackColor = System.Drawing.Color.Transparent;
            this.statisticsButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            
            this.menuLable.BackColor = System.Drawing.Color.Transparent;
            this.FormClosing += closer;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void passwordLable_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            new Menu.statistics(this._socket).ShowDialog();
            this.Visible = true;
        }

        private void createRoomButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            new Menu.CreateRoomScreen(this._socket).ShowDialog();
            this.Visible = true;
        }

        private void joinRoomButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            new Menu.JoinRoomScreen(this._socket).ShowDialog();
            this.Visible = true;
        }

        void closer(object sender, FormClosingEventArgs e)
        {
            this.signout();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.signout();
            this.Visible = false;
            new login.LoginScreen().ShowDialog();
            this.Close();
        }

        private void signout()
        {
            const int CODE = 5;

            string len = new string((char)0, 4);
            this._socket.sendMsg(((char)CODE).ToString() + len);
        }
    }
}
