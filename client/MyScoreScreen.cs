﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;


namespace Menu
{
    public partial class MyScoreScreen : Form
    {
        private client.Socket _socket = null;

        public MyScoreScreen(client.Socket socket)
        {
            this._socket = socket;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            const int CODE = 14;
            const string ERROR = "0";

            string len = new string((char)0, 4);
            this._socket.sendMsg(((char)CODE).ToString() + len);

            // get the response from the server
            // decode it to string and print the status
            getStatisticsResponse response = client.BsonConverts.fromBson<getStatisticsResponse>(this._socket.getMsg());

            var stats = response.statistics.Split(',').ToList();

            // set the title to the user name
            this.label1.Text = stats[6] + "'s " + this.label1.Text;

            // set the stats of the user according to the server response
            if (response.status != ERROR)
            {
                this.label2.Text += stats[0];
                this.label3.Text += stats[1];
                this.label4.Text += stats[2];
                this.label5.Text += stats[3];
                this.label6.Text += stats[4];
                this.label7.Text += stats[5];
            }
            else
            {
                this.Visible = false;
                new Exception.Exception("Coudln't recieve your stats").ShowDialog();
                this.Close();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    class getStatisticsResponse
    {
        public string status;
        public string statistics;
    }
}
