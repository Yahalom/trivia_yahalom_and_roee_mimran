﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace signup
{
    public partial class SignupScreen : Form
    {
        private client.Socket _socket = null;
        private login.LoginScreen _loginScreen = null;

        public SignupScreen(client.Socket socket, login.LoginScreen loginScreen)
        {
            this._socket = socket;
            this._loginScreen = loginScreen;

            this.BackgroundImage = client.Properties.Resources.background;

            InitializeComponent();
            this.Width = this.BackgroundImage.Width;
            this.Height = this.BackgroundImage.Height + 40;

            // static window size
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            usernameLable.BackColor = System.Drawing.Color.Transparent;
            passwordLable.BackColor = System.Drawing.Color.Transparent;
            emailLable.BackColor = System.Drawing.Color.Transparent;
            login.BackColor = System.Drawing.Color.Transparent;
            loginButton.BackColor = System.Drawing.Color.Transparent;
            signup.BackColor = System.Drawing.Color.Transparent;

            usernameLable.ForeColor = Color.Beige;
            passwordLable.ForeColor = Color.Beige;
            emailLable.ForeColor = Color.Beige;

            passwordTextBox.UseSystemPasswordChar = true;
            loginButton.Font = new Font(loginButton.Font.Name, loginButton.Font.SizeInPoints, FontStyle.Underline | FontStyle.Bold);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void usernameLable_Click(object sender, EventArgs e)
        {

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        private void signup_Click(object sender, EventArgs e)
        {
            const int CODE = 4;
            const int ERROR = 0;

            User user = new User
            {
                username = usernameTextBox.Text,
                password = passwordTextBox.Text,
                email = emailTextBox.Text
            };

            // create the bson and send it with the code msg
            this._socket.sendMsg((char)CODE + client.BsonConverts.toBson<User>(user));

            // get the response from the server
            string response = this._socket.getMsg();

            // decode it to string and print the status
            SignupResponse status = client.BsonConverts.fromBson<SignupResponse>(response);

            if (status.status == ERROR)
            {
                new Exception.Exception("Coudln't sign up").ShowDialog();
                System.Environment.Exit(1);
            }
            else
            {
                this.Visible = false;
                new Menu.MainMenuScreen(this._socket).ShowDialog();
            }
            System.Environment.Exit(1);
        }
    }

    public class User
    {
        public string username;
        public string password;
        public string email;
    }

    public class SignupResponse
    {
        public uint status;
    }
}
