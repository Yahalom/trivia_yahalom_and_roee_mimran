﻿namespace signup
{
    partial class SignupScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignupScreen));
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.usernameLable = new System.Windows.Forms.Label();
            this.passwordLable = new System.Windows.Forms.Label();
            this.emailLable = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.loginButton = new System.Windows.Forms.Label();
            this.login = new System.Windows.Forms.Label();
            this.signup = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.signup)).BeginInit();
            this.SuspendLayout();
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(345, 140);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(187, 20);
            this.usernameTextBox.TabIndex = 0;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(345, 223);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(187, 20);
            this.passwordTextBox.TabIndex = 1;
            // 
            // usernameLable
            // 
            this.usernameLable.AutoSize = true;
            this.usernameLable.BackColor = System.Drawing.SystemColors.Window;
            this.usernameLable.Font = new System.Drawing.Font("Snap ITC", 30F);
            this.usernameLable.Location = new System.Drawing.Point(51, 114);
            this.usernameLable.Name = "usernameLable";
            this.usernameLable.Size = new System.Drawing.Size(253, 51);
            this.usernameLable.TabIndex = 2;
            this.usernameLable.Text = "Username:";
            this.usernameLable.Click += new System.EventHandler(this.usernameLable_Click);
            // 
            // passwordLable
            // 
            this.passwordLable.AutoSize = true;
            this.passwordLable.Font = new System.Drawing.Font("Snap ITC", 30F);
            this.passwordLable.Location = new System.Drawing.Point(51, 197);
            this.passwordLable.Name = "passwordLable";
            this.passwordLable.Size = new System.Drawing.Size(255, 51);
            this.passwordLable.TabIndex = 3;
            this.passwordLable.Text = "Password:";
            // 
            // emailLable
            // 
            this.emailLable.AutoSize = true;
            this.emailLable.Font = new System.Drawing.Font("Snap ITC", 30F);
            this.emailLable.Location = new System.Drawing.Point(150, 279);
            this.emailLable.Name = "emailLable";
            this.emailLable.Size = new System.Drawing.Size(154, 51);
            this.emailLable.TabIndex = 7;
            this.emailLable.Text = "email:";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(345, 305);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(187, 20);
            this.emailTextBox.TabIndex = 6;
            // 
            // loginButton
            // 
            this.loginButton.AutoSize = true;
            this.loginButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loginButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.loginButton.Location = new System.Drawing.Point(461, 360);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(141, 27);
            this.loginButton.TabIndex = 8;
            this.loginButton.Text = "Login here.";
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // login
            // 
            this.login.AutoSize = true;
            this.login.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.login.Location = new System.Drawing.Point(131, 360);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(311, 27);
            this.login.TabIndex = 9;
            this.login.Text = "Already have an account?";
            // 
            // signup
            // 
            this.signup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signup.Image = ((System.Drawing.Image)(resources.GetObject("signup.Image")));
            this.signup.Location = new System.Drawing.Point(275, 401);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(232, 50);
            this.signup.TabIndex = 10;
            this.signup.TabStop = false;
            this.signup.Click += new System.EventHandler(this.signup_Click);
            // 
            // SignupScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 492);
            this.Controls.Add(this.signup);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.login);
            this.Controls.Add(this.emailLable);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.passwordLable);
            this.Controls.Add(this.usernameLable);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.usernameTextBox);
            this.Name = "SignupScreen";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.signup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label usernameLable;
        private System.Windows.Forms.Label passwordLable;
        private System.Windows.Forms.Label emailLable;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label loginButton;
        private System.Windows.Forms.Label login;
        private System.Windows.Forms.PictureBox signup;
    }
}
