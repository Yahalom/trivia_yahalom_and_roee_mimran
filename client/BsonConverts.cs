﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace client
{
    class BsonConverts
    {
        public static string toBson<T>(T request)
        {
            MemoryStream ms = new MemoryStream();
            using (BsonWriter writer = new BsonWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, request);
            }

            byte[] buffer = ms.ToArray();
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }


        public static T fromBson<T>(string response)
        {
            response = response.Substring(1); // remove the code
            byte[] data = Encoding.ASCII.GetBytes(response);

            MemoryStream ms = new MemoryStream(data);
            using (BsonReader reader = new BsonReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();

                T e = serializer.Deserialize<T>(reader);
                return e;
            }
        }
    }
}
