﻿namespace Menu
{
    partial class TopScoreScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Rank1 = new System.Windows.Forms.Label();
            this.Rank2 = new System.Windows.Forms.Label();
            this.Rank3 = new System.Windows.Forms.Label();
            this.Name1 = new System.Windows.Forms.Label();
            this.Name2 = new System.Windows.Forms.Label();
            this.Name3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.WonGames1 = new System.Windows.Forms.Label();
            this.WonGames2 = new System.Windows.Forms.Label();
            this.WonGames3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(859, 87);
            this.label1.TabIndex = 0;
            this.label1.Text = "Top Score";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Window;
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(158, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 36);
            this.label2.TabIndex = 1;
            this.label2.Text = "Rank";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Window;
            this.label4.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(370, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 36);
            this.label4.TabIndex = 1;
            this.label4.Text = "Name";
            // 
            // Rank1
            // 
            this.Rank1.AutoSize = true;
            this.Rank1.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.Rank1.Location = new System.Drawing.Point(185, 165);
            this.Rank1.Name = "Rank1";
            this.Rank1.Size = new System.Drawing.Size(36, 36);
            this.Rank1.TabIndex = 2;
            this.Rank1.Text = "1.";
            // 
            // Rank2
            // 
            this.Rank2.AutoSize = true;
            this.Rank2.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.Rank2.Location = new System.Drawing.Point(185, 228);
            this.Rank2.Name = "Rank2";
            this.Rank2.Size = new System.Drawing.Size(37, 36);
            this.Rank2.TabIndex = 2;
            this.Rank2.Text = "2.";
            // 
            // Rank3
            // 
            this.Rank3.AutoSize = true;
            this.Rank3.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.Rank3.Location = new System.Drawing.Point(185, 304);
            this.Rank3.Name = "Rank3";
            this.Rank3.Size = new System.Drawing.Size(38, 36);
            this.Rank3.TabIndex = 2;
            this.Rank3.Text = "3.";
            // 
            // Name1
            // 
            this.Name1.AutoSize = true;
            this.Name1.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.Name1.Location = new System.Drawing.Point(360, 165);
            this.Name1.Name = "Name1";
            this.Name1.Size = new System.Drawing.Size(0, 36);
            this.Name1.TabIndex = 2;
            // 
            // Name2
            // 
            this.Name2.AutoSize = true;
            this.Name2.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.Name2.Location = new System.Drawing.Point(360, 228);
            this.Name2.Name = "Name2";
            this.Name2.Size = new System.Drawing.Size(0, 36);
            this.Name2.TabIndex = 2;
            // 
            // Name3
            // 
            this.Name3.AutoSize = true;
            this.Name3.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.Name3.Location = new System.Drawing.Point(360, 304);
            this.Name3.Name = "Name3";
            this.Name3.Size = new System.Drawing.Size(0, 36);
            this.Name3.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::client.Properties.Resources.backArrow;
            this.pictureBox1.Location = new System.Drawing.Point(819, 424);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 31);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Window;
            this.label3.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(568, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 36);
            this.label3.TabIndex = 1;
            this.label3.Text = "Games Won";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // WonGames1
            // 
            this.WonGames1.AutoSize = true;
            this.WonGames1.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.WonGames1.Location = new System.Drawing.Point(577, 165);
            this.WonGames1.Name = "WonGames1";
            this.WonGames1.Size = new System.Drawing.Size(0, 36);
            this.WonGames1.TabIndex = 2;
            // 
            // WonGames2
            // 
            this.WonGames2.AutoSize = true;
            this.WonGames2.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.WonGames2.Location = new System.Drawing.Point(577, 228);
            this.WonGames2.Name = "WonGames2";
            this.WonGames2.Size = new System.Drawing.Size(0, 36);
            this.WonGames2.TabIndex = 2;
            // 
            // WonGames3
            // 
            this.WonGames3.AutoSize = true;
            this.WonGames3.Font = new System.Drawing.Font("Showcard Gothic", 21.75F);
            this.WonGames3.Location = new System.Drawing.Point(577, 304);
            this.WonGames3.Name = "WonGames3";
            this.WonGames3.Size = new System.Drawing.Size(0, 36);
            this.WonGames3.TabIndex = 2;
            // 
            // TopScoreScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(859, 467);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.WonGames3);
            this.Controls.Add(this.Name3);
            this.Controls.Add(this.WonGames2);
            this.Controls.Add(this.Name2);
            this.Controls.Add(this.Rank3);
            this.Controls.Add(this.WonGames1);
            this.Controls.Add(this.Name1);
            this.Controls.Add(this.Rank2);
            this.Controls.Add(this.Rank1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "TopScoreScreen";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Rank1;
        private System.Windows.Forms.Label Rank2;
        private System.Windows.Forms.Label Rank3;
        private System.Windows.Forms.Label Name1;
        private System.Windows.Forms.Label Name2;
        private System.Windows.Forms.Label Name3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label WonGames1;
        private System.Windows.Forms.Label WonGames2;
        private System.Windows.Forms.Label WonGames3;
    }
}

