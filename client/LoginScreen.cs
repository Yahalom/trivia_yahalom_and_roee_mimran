﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login
{
    public partial class LoginScreen : Form
    {
        private client.Socket _socket = null;

        public LoginScreen()
        {
            try
            {
                this._socket = new client.Socket();

            }
            catch
            {
                // show the error and exit from the gui
                new Exception.Exception("Communication Error").ShowDialog();
                System.Environment.Exit(1);
            }

            this.BackgroundImage = client.Properties.Resources.background;

            InitializeComponent();
            this.Width = this.BackgroundImage.Width;
            this.Height = this.BackgroundImage.Height + 40;

            // static window size
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            usernameLable.BackColor = System.Drawing.Color.Transparent;
            passwordLable.BackColor = System.Drawing.Color.Transparent;
            signupButton.BackColor = System.Drawing.Color.Transparent;
            signup.BackColor = System.Drawing.Color.Transparent;

            usernameLable.ForeColor = Color.Beige;
            passwordLable.ForeColor = Color.Beige;

            passwordTextBox.UseSystemPasswordChar = true;
            signupButton.Font = new Font(signupButton.Font.Name, signupButton.Font.SizeInPoints, FontStyle.Underline | FontStyle.Bold);

            login.BackColor = System.Drawing.Color.Transparent;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void usernameLable_Click(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void signupButton_Click_1(object sender, EventArgs e)
        {
            this.Visible = false;
            new signup.SignupScreen(this._socket, this).ShowDialog();
            this.Visible = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            const int CODE = 3;
            const int ERROR = 0;

            User user = new User
            {
                username = usernameTextBox.Text,
                password = passwordTextBox.Text,
            };

            // create the bson and send it with the code msg
            this._socket.sendMsg((char)CODE + client.BsonConverts.toBson<User>(user));

            // get the response from the server
            string response = this._socket.getMsg();

            // decode it to string and print the status
            SignupResponse status = client.BsonConverts.fromBson<SignupResponse>(response);

            if (status.status == ERROR)
            {
                new Exception.Exception("Couldn't log in").ShowDialog();
            }
            else
            {
                this.Visible = false;
                new Menu.MainMenuScreen(this._socket).ShowDialog();
                this.Close();
            }
        }
    }

    public class User
    {
        public string username;
        public string password;
    }

    public class SignupResponse
    {
        public uint status;
    }
}
