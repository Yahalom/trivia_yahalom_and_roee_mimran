﻿namespace Menu
{
    partial class statistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.PictureBox();
            this.myScore = new System.Windows.Forms.PictureBox();
            this.highScore = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myScore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.highScore)).BeginInit();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Image = global::client.Properties.Resources.back;
            this.back.Location = new System.Drawing.Point(291, 505);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(279, 100);
            this.back.TabIndex = 0;
            this.back.TabStop = false;
            this.back.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // myScore
            // 
            this.myScore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.myScore.Image = global::client.Properties.Resources.myScore;
            this.myScore.Location = new System.Drawing.Point(516, 86);
            this.myScore.Name = "myScore";
            this.myScore.Size = new System.Drawing.Size(338, 137);
            this.myScore.TabIndex = 0;
            this.myScore.TabStop = false;
            this.myScore.Click += new System.EventHandler(this.myScore_Click);
            // 
            // highScore
            // 
            this.highScore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.highScore.Image = global::client.Properties.Resources.highScore;
            this.highScore.Location = new System.Drawing.Point(12, 86);
            this.highScore.Name = "highScore";
            this.highScore.Size = new System.Drawing.Size(337, 137);
            this.highScore.TabIndex = 0;
            this.highScore.TabStop = false;
            this.highScore.Click += new System.EventHandler(this.highScore_Click);
            // 
            // statistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(866, 617);
            this.Controls.Add(this.back);
            this.Controls.Add(this.myScore);
            this.Controls.Add(this.highScore);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "statistics";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myScore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.highScore)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox highScore;
        private System.Windows.Forms.PictureBox myScore;
        private System.Windows.Forms.PictureBox back;
    }
}

